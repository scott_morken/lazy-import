<?php

declare(strict_types=1);

namespace Tests\Smorken\LazyImport\Unit\Models\Concerns;

use PHPUnit\Framework\Attributes\Test;
use PHPUnit\Framework\TestCase;
use Smorken\Model\Eloquent;
use Tests\Smorken\LazyImport\Concerns\WithMockConnection;
use Tests\Smorken\LazyImport\Stubs\IdentifiersBuilderStub;

class HasIdentifiersOnBuilderTest extends TestCase
{
    use WithMockConnection;

    #[Test]
    public function it_creates_a_query_for_identifierIn(): void
    {
        $sut = new class extends Eloquent
        {
            protected static string $builder = IdentifiersBuilderStub::class;

            protected $table = 't';
        };
        $query = $sut->newQuery();
        $query->identifierIn([
            ['foo' => 'foo 1', 'bar' => 'bar 1'],
            ['foo' => 'foo 2', 'bar' => 'bar 2'],
        ]);
        $this->assertEquals('select * from `t` where ((`foo` = ? and `bar` = ?) or (`foo` = ? and `bar` = ?))',
            $query->toSql());
        $this->assertEquals(['foo 1', 'bar 1', 'foo 2', 'bar 2'], $query->getBindings());
    }

    #[Test]
    public function it_creates_a_query_for_identifierIs(): void
    {
        $sut = new class extends Eloquent
        {
            protected static string $builder = IdentifiersBuilderStub::class;

            protected $table = 't';
        };
        $query = $sut->newQuery();
        $query->identifierIs('foo', 'foo_value');
        $this->assertEquals('select * from `t` where `foo` = ?', $query->toSql());
        $this->assertEquals(['foo_value'], $query->getBindings());
    }

    #[Test]
    public function it_creates_a_query_for_identifiers(): void
    {
        $sut = new class extends Eloquent
        {
            protected static string $builder = IdentifiersBuilderStub::class;

            protected $table = 't';
        };
        $query = $sut->newQuery();
        $query->identifiers([
            ['foo' => 'f 1'],
            ['foo' => 'f 2'],
            ['foo' => 'foo 1', 'bar' => 'bar 1'],
            ['foo' => 'foo 2', 'bar' => 'bar 2'],
        ]);
        $this->assertEquals('select * from `t` where (`foo` = ? or `foo` = ? or (`foo` = ? and `bar` = ?) or (`foo` = ? and `bar` = ?))',
            $query->toSql());
        $this->assertEquals(['f 1', 'f 2', 'foo 1', 'bar 1', 'foo 2', 'bar 2'], $query->getBindings());
    }

    protected function setUp(): void
    {
        parent::setUp();
        $this->initConnectionResolver();
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        \Mockery::close();
    }
}
