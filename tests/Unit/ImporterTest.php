<?php

declare(strict_types=1);

namespace Tests\Smorken\LazyImport\Unit;

use Illuminate\Container\Container;
use Illuminate\Events\Dispatcher;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\LazyCollection;
use Illuminate\Support\MessageBag;
use Mockery as m;
use PHPUnit\Framework\Attributes\Test;
use PHPUnit\Framework\TestCase;
use Smorken\LazyImport\Actions\Results\ImportResult;
use Smorken\LazyImport\Contracts\Actions\ImportAction;
use Smorken\LazyImport\Contracts\Data\TargetData;
use Smorken\LazyImport\Contracts\Repositories\SourceRepository;
use Smorken\LazyImport\Importer;
use Tests\Smorken\LazyImport\Concerns\HasFooMap;
use Tests\Smorken\LazyImport\Concerns\WithCacheManager;
use Tests\Smorken\LazyImport\Stubs\ModelStub;

class ImporterTest extends TestCase
{
    use HasFooMap, WithCacheManager;

    #[Test]
    public function it_imports_values(): void
    {
        Event::fake();
        $map = $this->getFooMap();
        $sut = new Importer(
            m::mock(SourceRepository::class),
            $map,
            m::mock(ImportAction::class),
        );
        $sut->getSourceRepository()->expects()
            ->__invoke(null)
            ->andReturns(new LazyCollection([
                new ModelStub(['foo_id' => 1, 'foo_name' => 'f 1']),
                new ModelStub(['foo_id' => 2, 'foo_name' => 'f 2']),
                new ModelStub(['foo_id' => 3, 'foo_name' => 'f 3']),
                new ModelStub(['foo_id' => 4, 'foo_name' => 'f 4']),
                new ModelStub(['foo_id' => 5, 'foo_name' => 'f 5']),
            ]));
        $sut->getImportAction()->shouldReceive('__invoke')
            ->twice()
            ->withArgs(function (Collection $mapped) {
                return count($mapped) === 2;
            })
            ->andReturns(
                new ImportResult(
                    1,
                    1,
                    1,
                    0,
                ),
                new ImportResult(
                    2,
                    0,
                    1,
                    1,
                ),
            );
        $sut->getImportAction()->shouldReceive('__invoke')
            ->once()
            ->withArgs(function (Collection $mapped) {
                return count($mapped) === 1;
            })
            ->andReturns(
                new ImportResult(
                    0,
                    1,
                    0,
                    0,
                ),
            );
        $sut->getImportAction()->expects()
            ->getMessages()
            ->andReturns(new MessageBag);
        $results = $sut->import(null, 2);
        $this->assertEquals([
            'Importer::total' => 5,
            'Importer::existing' => 3,
            'Importer::created' => 2,
            'Importer::updated' => 2,
            'Importer::touched' => 1,
            'Importer::validated' => 5,
        ], $results->getCounters());
    }

    #[Test]
    public function it_samples_values(): void
    {
        $map = $this->getFooMap();
        $sut = new Importer(
            m::mock(SourceRepository::class),
            $map,
            m::mock(ImportAction::class),
        );
        $sut->getSourceRepository()->expects()
            ->__invoke(null)
            ->andReturns(new LazyCollection(function () {
                foreach ([
                    new ModelStub(['foo_id' => 1, 'foo_name' => 'f 1']),
                    new ModelStub(['foo_id' => 2, 'foo_name' => 'f 2']),
                    new ModelStub(['foo_id' => 3, 'foo_name' => 'f 3']),
                    new ModelStub(['foo_id' => 4, 'foo_name' => 'f 4']),
                ] as $m) {
                    yield $m;
                }
            }));
        $results = $sut->sample(null, 2)->map(fn (TargetData $t) => $t->toArray())->toArray();
        $this->assertEquals([
            [
                'id' => 1,
                'name' => 'f 1',
                'description' => 'some description',
                'stub_at' => null,
            ],
            [
                'id' => 2,
                'name' => 'f 2',
                'description' => 'some description',
                'stub_at' => null,
            ],
        ], $results);
    }

    protected function setUp(): void
    {
        parent::setUp();
        $c = Container::getInstance();
        $dispatcher = new Dispatcher($c);
        $c->bind('events', fn () => $dispatcher);
        $c->bind('cache', fn () => $this->getCacheManager());
        Event::setFacadeApplication($c);
        $this->getCacheManager()->allows()->refreshEventDispatcher();
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        m::close();
    }
}
