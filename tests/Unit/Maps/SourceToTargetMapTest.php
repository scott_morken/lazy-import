<?php

declare(strict_types=1);

namespace Tests\Smorken\LazyImport\Unit\Maps;

use PHPUnit\Framework\Attributes\Test;
use PHPUnit\Framework\TestCase;
use Smorken\LazyImport\Exceptions\LazyImportException;
use Tests\Smorken\LazyImport\Stubs\TargetDataStub;

class SourceToTargetMapTest extends TestCase
{
    #[Test]
    public function it_can_create_target_data(): void
    {
        $sut = new class(TargetDataStub::class, ['other' => 1, 'short_descr' => 'foo', 'other_descr' => 'bar']) extends \Smorken\LazyImport\Maps\SourceToTargetMap
        {
            protected array|string|null $sourceIdentifierKey = 'other';

            protected array|string|null $targetIdentifierKey = 'id';

            protected function getTargetAndSourceMap(): array
            {
                return [
                    'id' => 'other',
                    'name' => 'short_descr',
                    'description' => fn (array $model) => $model['short_descr'].' '.$model['other_descr'],
                ];
            }
        };
        $this->assertEquals([
            'id' => 1,
            'name' => 'foo',
            'description' => 'foo bar',
            'stub_at' => null,
        ], $sut->toTargetData()->toArray());
    }

    #[Test]
    public function it_can_create_target_data_with_arrays(): void
    {
        $sut = new class(TargetDataStub::class, ['id' => 'abc', 'other_id' => 1]) extends \Smorken\LazyImport\Maps\SourceToTargetMap
        {
            protected array|string|null $sourceIdentifierKey = ['id', 'other_id'];

            protected array|string|null $targetIdentifierKey = ['s_id', 's_other_id'];

            protected function getTargetAndSourceMap(): array
            {
                return [
                    's_id' => 'id',
                    's_other_id' => 'other_id',
                ];
            }
        };
        $this->assertEquals(['s_id' => 'abc', 's_other_id' => 1], $sut->identifier(false));
    }

    #[Test]
    public function it_is_not_a_valid_source_when_identifier_is_null(): void
    {
        $sut = new class(TargetDataStub::class, ['id1' => 'foo']) extends \Smorken\LazyImport\Maps\SourceToTargetMap
        {
            protected array|string|null $sourceIdentifierKey = ['id1', 'id2'];

            protected function getTargetAndSourceMap(): array
            {
                return [];
            }
        };
        $this->assertFalse($sut->validSource());
    }

    #[Test]
    public function it_is_not_a_valid_source_when_identifiers_are_not_null(): void
    {
        $sut = new class(TargetDataStub::class, ['id1' => 'foo', 'id2' => 0]) extends \Smorken\LazyImport\Maps\SourceToTargetMap
        {
            protected array|string|null $sourceIdentifierKey = ['id1', 'id2'];

            protected function getTargetAndSourceMap(): array
            {
                return [];
            }
        };
        $this->assertTrue($sut->validSource());
    }

    #[Test]
    public function it_returns_a_source_identifier_array_for_an_array_source_model(): void
    {
        $sut = new class(TargetDataStub::class, ['id1' => 'foo', 'id2' => 'bar']) extends \Smorken\LazyImport\Maps\SourceToTargetMap
        {
            protected array|string|null $sourceIdentifierKey = ['id1', 'id2'];

            protected function getTargetAndSourceMap(): array
            {
                return [];
            }
        };
        $this->assertEquals('foo:bar', $sut->identifier());
        $this->assertEquals(['id1' => 'foo', 'id2' => 'bar'], $sut->identifier(false));
    }

    #[Test]
    public function it_returns_a_source_identifier_string_for_an_array_source_model(): void
    {
        $sut = new class(TargetDataStub::class, ['id' => 'foo']) extends \Smorken\LazyImport\Maps\SourceToTargetMap
        {
            protected array|string|null $sourceIdentifierKey = 'id';

            protected function getTargetAndSourceMap(): array
            {
                return [];
            }
        };
        $this->assertEquals('foo', $sut->identifier());
        $this->assertEquals(['id' => 'foo'], $sut->identifier(false));
    }

    #[Test]
    public function it_returns_a_source_identifier_string_with_a_different_target_key_for_an_array_source_model(): void
    {
        $sut = new class(TargetDataStub::class, ['id' => 'foo']) extends \Smorken\LazyImport\Maps\SourceToTargetMap
        {
            protected array|string|null $sourceIdentifierKey = 'id';

            protected array|string|null $targetIdentifierKey = 'foo_id';

            protected function getTargetAndSourceMap(): array
            {
                return [];
            }
        };
        $this->assertEquals('foo', $sut->identifier());
        $this->assertEquals(['foo_id' => 'foo'], $sut->identifier(false));
    }

    #[Test]
    public function it_throws_an_exception_without_source_identifier_keys(): void
    {
        $sut = new class(TargetDataStub::class) extends \Smorken\LazyImport\Maps\SourceToTargetMap
        {
            protected function getTargetAndSourceMap(): array
            {
                return [];
            }
        };
        $this->expectException(LazyImportException::class);
        $sut->identifier();
    }
}
