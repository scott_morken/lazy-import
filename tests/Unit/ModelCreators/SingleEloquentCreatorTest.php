<?php

declare(strict_types=1);

namespace Tests\Smorken\LazyImport\Unit\ModelCreators;

use Illuminate\Database\Eloquent\Model;
use Mockery as m;
use PHPUnit\Framework\Attributes\Test;
use PHPUnit\Framework\TestCase;
use Smorken\LazyImport\ModelCreators\SingleEloquentCreator;
use Tests\Smorken\LazyImport\Stubs\TargetDataStub;

class SingleEloquentCreatorTest extends TestCase
{
    #[Test]
    public function it_is_false_when_there_is_nothing_to_create(): void
    {
        $m = new class extends Model
        {
            protected $fillable = ['id', 'name', 'description', 'stub_at'];
        };
        $model = m::mock($m)->makePartial();
        $sut = new SingleEloquentCreator($model);
        $this->assertFalse($sut->tryCreate());
    }

    #[Test]
    public function it_is_one_when_a_record_is_successfully_created(): void
    {
        $m = new class extends Model
        {
            protected $fillable = ['id', 'name', 'description', 'stub_at'];
        };
        $model = m::mock($m)->makePartial();
        $savableModel = m::mock(Model::class);
        $savableModel->expects()->save()->andReturns(true);
        $model->expects()
            ->newInstance(['id' => 1, 'name' => 'foo', 'description' => 'foo description', 'stub_at' => null])
            ->andReturns($savableModel);
        $target = new TargetDataStub(
            ['id' => 1],
            1,
            'foo',
            'foo description'
        );
        $sut = new SingleEloquentCreator($model);
        $sut->add($target);
        $this->assertEquals(1, $sut->tryCreate());
    }

    #[Test]
    public function it_resets_the_target(): void
    {
        $m = new class extends Model
        {
            protected $fillable = ['id', 'name', 'description', 'stub_at'];
        };
        $model = m::mock($m)->makePartial();
        $savableModel = m::mock(Model::class);
        $savableModel->expects()->save()->andReturns(true);
        $model->expects()
            ->newInstance(['id' => 1, 'name' => 'foo', 'description' => 'foo description', 'stub_at' => null])
            ->andReturns($savableModel);
        $target = new TargetDataStub(
            ['id' => 1],
            1,
            'foo',
            'foo description'
        );
        $sut = new SingleEloquentCreator($model);
        $sut->add($target);
        $this->assertEquals(1, $sut->tryCreate());
        $this->assertFalse($sut->tryCreate(true));
    }

    #[Test]
    public function it_is_zero_when_a_record_is_not_saved(): void
    {
        $m = new class extends Model
        {
            protected $fillable = ['id', 'name', 'description', 'stub_at'];
        };
        $model = m::mock($m)->makePartial();
        $savableModel = m::mock(Model::class);
        $savableModel->expects()->save()->andReturns(false);
        $model->expects()
            ->newInstance(['id' => 1, 'name' => 'foo', 'description' => 'foo description', 'stub_at' => null])
            ->andReturns($savableModel);
        $target = new TargetDataStub(
            ['id' => 1],
            1,
            'foo',
            'foo description'
        );
        $sut = new SingleEloquentCreator($model);
        $sut->add($target);
        $this->assertEquals(0, $sut->tryCreate());
        $messages = $sut->getMessages()->toArray();
        $this->assertStringStartsWith('Model: Mockery_', $messages['CreateError'][0]);
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        m::close();
    }
}
