<?php

declare(strict_types=1);

namespace Tests\Smorken\LazyImport\Unit\ModelCreators;

use Illuminate\Contracts\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Mockery as m;
use PHPUnit\Framework\Attributes\Test;
use PHPUnit\Framework\TestCase;
use Smorken\LazyImport\Data\BaseTargetData;
use Smorken\LazyImport\ModelCreators\BatchEloquentCreator;
use Tests\Smorken\LazyImport\Stubs\TargetDataStub;

class BatchEloquentCreatorTest extends TestCase
{
    #[Test]
    public function it_adds_an_error_when_not_forcing_and_batch_size_has_not_been_set(): void
    {
        $m = new class extends Model
        {
            protected $fillable = ['id', 'name', 'description', 'stub_at'];
        };
        $model = m::mock($m)->makePartial();
        $target = new class(['id' => null]) extends BaseTargetData {}; // no attributes
        $sut = new BatchEloquentCreator($model);
        $sut->add($target);
        $this->assertFalse($sut->tryCreate());
        $this->assertEquals([
            'CreateError:batch-0' => ['No batch size has been set using attributes. Cannot create records.'],
        ], $sut->getMessages()->toArray());
    }

    #[Test]
    public function it_creates_records_when_gte_batch_size(): void
    {
        $m = new class extends Model
        {
            protected $fillable = ['id', 'name', 'description', 'stub_at'];
        };
        $model = m::mock($m)->makePartial();
        $query = m::mock(Builder::class);
        $model->expects()->query()->twice()->andReturns($query);
        $query->expects()->insertOrIgnore(m::type('array'))->twice()->andReturnUsing(
            function (array $creating) {
                $expected = [
                    [
                        'id' => 1,
                        'name' => 'foo 1',
                        'description' => 'foo 1 description',
                        'stub_at' => null,
                    ],
                    [
                        'id' => 2,
                        'name' => 'foo 2',
                        'description' => 'foo 2 description',
                        'stub_at' => null,
                    ],
                ];
                $this->assertCount(2, $creating);
                foreach ($expected as $i => $attributes) {
                    foreach ($attributes as $k => $v) {
                        $this->assertEquals($v, $creating[$i][$k], "(first) i: $i, k: $k");
                    }
                }

                return count($creating);
            },
            function (array $creating) {
                $expected = [
                    [
                        'id' => 3,
                        'name' => 'foo 3',
                        'description' => 'foo 3 description',
                        'stub_at' => null,
                    ],
                ];
                $this->assertCount(1, $creating);
                foreach ($expected as $i => $attributes) {
                    foreach ($attributes as $k => $v) {
                        $this->assertEquals($v, $creating[$i][$k], "(second) i: $i, k: $k");
                    }
                }

                return count($creating);
            }
        );
        $targets = [
            new TargetDataStub(
                ['id' => 1],
                1,
                'foo 1',
                'foo 1 description'
            ),
            new TargetDataStub(
                ['id' => 2],
                2,
                'foo 2',
                'foo 2 description'
            ),
            new TargetDataStub(
                ['id' => 3],
                3,
                'foo 3',
                'foo 3 description'
            ),
        ];
        $sut = new BatchEloquentCreator($model, 2);
        $count = 0;
        foreach ($targets as $target) {
            $sut->add($target);
            $count += $sut->tryCreate() ?: 0;
        }
        $this->assertEquals(2, $count);
        $count += $sut->tryCreate(true) ?: 0;
        $this->assertEquals(3, $count);
        $this->assertEquals([], $sut->getMessages()->toArray());
    }

    #[Test]
    public function it_is_false_when_there_are_no_targets_to_create(): void
    {
        $m = new class extends Model
        {
            protected $fillable = ['id', 'name', 'description', 'stub_at'];
        };
        $model = m::mock($m)->makePartial();
        $sut = new BatchEloquentCreator($model);
        $this->assertFalse($sut->tryCreate());
        $this->assertEquals([], $sut->getMessages()->toArray());
    }

    #[Test]
    public function it_returns_count_when_count_less_than_batch_size_and_force_is_true(): void
    {
        $m = new class extends Model
        {
            protected $fillable = ['id', 'name', 'description', 'stub_at'];
        };
        $model = m::mock($m)->makePartial();
        $query = m::mock(Builder::class);
        $model->expects()->query()->andReturns($query);
        $query->expects()->insertOrIgnore(m::type('array'))->andReturnUsing(
            function (array $creating) {
                $expected = [
                    [
                        'id' => 1,
                        'name' => 'foo',
                        'description' => 'foo description',
                        'stub_at' => null,
                    ],
                ];
                $this->assertCount(1, $creating);
                foreach ($expected as $i => $attributes) {
                    foreach ($attributes as $k => $v) {
                        $this->assertEquals($v, $creating[$i][$k], "i: $i, k: $k");
                    }
                }

                return count($creating);
            }
        );
        $target = new TargetDataStub(
            ['id' => 1],
            1,
            'foo',
            'foo description'
        );
        $sut = new BatchEloquentCreator($model);
        $sut->add($target);
        $this->assertEquals(1, $sut->tryCreate(true));
        $this->assertEquals([], $sut->getMessages()->toArray());
    }

    #[Test]
    public function it_returns_false_when_count_less_than_batch_size(): void
    {
        $m = new class extends Model
        {
            protected $fillable = ['id', 'name', 'description', 'stub_at'];
        };
        $model = m::mock($m)->makePartial();
        $target = new TargetDataStub(
            ['id' => 1],
            1,
            'foo',
            'foo description'
        );
        $sut = new BatchEloquentCreator($model);
        $sut->add($target);
        $this->assertFalse($sut->tryCreate());
        $this->assertEquals([], $sut->getMessages()->toArray());
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        m::close();
    }
}
