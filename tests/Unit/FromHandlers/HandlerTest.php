<?php

declare(strict_types=1);

namespace Tests\Smorken\LazyImport\Unit\FromHandlers;

use Illuminate\Container\Container;
use Illuminate\Events\Dispatcher;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\LazyCollection;
use Illuminate\Support\MessageBag;
use Mockery as m;
use PHPUnit\Framework\Attributes\Test;
use PHPUnit\Framework\TestCase;
use Smorken\LazyImport\Actions\Results\ImportResult;
use Smorken\LazyImport\Contracts\Actions\ImportAction;
use Smorken\LazyImport\FromHandlers\Handler;
use Tests\Smorken\LazyImport\Concerns\HasBarMap;
use Tests\Smorken\LazyImport\Concerns\HasFooMap;
use Tests\Smorken\LazyImport\Concerns\WithCacheManager;
use Tests\Smorken\LazyImport\Stubs\FromHandlers\RelatedCollectionConverter;
use Tests\Smorken\LazyImport\Stubs\ModelStub;

class HandlerTest extends TestCase
{
    use HasBarMap, HasFooMap, WithCacheManager;

    #[Test]
    public function it_imports_values(): void
    {
        Event::fake();
        $map = $this->getFooMap();
        $importAction = m::mock(ImportAction::class);
        $sut = new Handler(
            'Importer',
            $map,
            $importAction,
        );
        $lazyCollection = new LazyCollection([
            new ModelStub(['foo_id' => 1, 'foo_name' => 'f 1']),
            new ModelStub(['foo_id' => 2, 'foo_name' => 'f 2']),
            new ModelStub(['foo_id' => 3, 'foo_name' => 'f 3']),
            new ModelStub(['foo_id' => 4, 'foo_name' => 'f 4']),
            new ModelStub(['foo_id' => 5, 'foo_name' => 'f 5']),
        ]);
        $importAction->shouldReceive('__invoke')
            ->twice()
            ->withArgs(function (Collection $mapped) {
                return count($mapped) === 2;
            })
            ->andReturns(
                new ImportResult(
                    1,
                    1,
                    1,
                    0,
                ),
                new ImportResult(
                    2,
                    0,
                    1,
                    1,
                ),
            );
        $importAction->shouldReceive('__invoke')
            ->once()
            ->withArgs(function (Collection $mapped) {
                return count($mapped) === 1;
            })
            ->andReturns(
                new ImportResult(
                    0,
                    1,
                    0,
                    0,
                ),
            );
        $importAction->expects()
            ->getMessages()
            ->andReturns(new MessageBag);
        foreach ($lazyCollection as $item) {
            $sut->import($item, 2);
        }
        $sut->finalize(null);
        $result = $sut->getResult();
        $this->assertEquals([
            'Importer::total' => 5,
            'Importer::existing' => 3,
            'Importer::created' => 2,
            'Importer::updated' => 2,
            'Importer::touched' => 1,
            'Importer::validated' => 5,
        ], $result->getCounters());
    }

    #[Test]
    public function it_samples_values(): void
    {
        $map = $this->getFooMap();
        $importAction = m::mock(ImportAction::class);
        $sut = new Handler(
            'Importer',
            $map,
            $importAction,
        );
        $lazyCollection = new LazyCollection([
            new ModelStub(['foo_id' => 1, 'foo_name' => 'f 1']),
            new ModelStub(['foo_id' => 2, 'foo_name' => 'f 2']),
            new ModelStub(['foo_id' => 3, 'foo_name' => 'f 3']),
            new ModelStub(['foo_id' => 4, 'foo_name' => 'f 4']),
            new ModelStub(['foo_id' => 5, 'foo_name' => 'f 5']),
        ]);
        $sampled = new Collection;
        foreach ($lazyCollection as $item) {
            $sampled = $sampled->merge($sut->sample($item));
        }
        $this->assertEquals([
            [
                'identifier' => ['id' => 1],
                'id' => 1,
                'name' => 'f 1',
                'description' => 'some description',
                'stub_at' => null,
            ],
            [
                'identifier' => ['id' => 2],
                'id' => 2,
                'name' => 'f 2',
                'description' => 'some description',
                'stub_at' => null,
            ],
            [
                'identifier' => ['id' => 3],
                'id' => 3,
                'name' => 'f 3',
                'description' => 'some description',
                'stub_at' => null,
            ],
            [
                'identifier' => ['id' => 4],
                'id' => 4,
                'name' => 'f 4',
                'description' => 'some description',
                'stub_at' => null,
            ],
            [
                'identifier' => ['id' => 5],
                'id' => 5,
                'name' => 'f 5',
                'description' => 'some description',
                'stub_at' => null,
            ],
        ], $sampled->map(fn ($m) => $m->map())->toArray());
    }

    #[Test]
    public function it_samples_values_from_a_related_collection(): void
    {
        $map = $this->getBarMap();
        $importAction = m::mock(ImportAction::class);
        $sut = new Handler(
            'Importer',
            $map,
            $importAction,
            null,
            new RelatedCollectionConverter
        );
        $lazyCollection = new LazyCollection([
            new ModelStub(['foo_id' => 1, 'foo_name' => 'f 1']),
            new ModelStub(['foo_id' => 2, 'foo_name' => 'f 2']),
        ]);
        $sampled = new Collection;
        foreach ($lazyCollection as $item) {
            $sampled = $sampled->merge($sut->sample($item));
        }
        $this->assertEquals([
            [
                'identifier' => ['id' => '1-a'],
                'id' => '1-a',
                'name' => 'f 1-a',
            ],
            [
                'identifier' => ['id' => '1-b'],
                'id' => '1-b',
                'name' => 'f 1-b',
            ],
            [
                'identifier' => ['id' => '2-a'],
                'id' => '2-a',
                'name' => 'f 2-a',
            ],
            [
                'identifier' => ['id' => '2-b'],
                'id' => '2-b',
                'name' => 'f 2-b',
            ],
        ], $sampled->map(fn ($m) => $m->map())->toArray());
    }

    protected function setUp(): void
    {
        parent::setUp();
        $c = Container::getInstance();
        $dispatcher = new Dispatcher($c);
        $c->bind('events', fn () => $dispatcher);
        $c->bind('cache', fn () => $this->getCacheManager());
        Event::setFacadeApplication($c);
        $this->getCacheManager()->allows()->refreshEventDispatcher();
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        m::close();
    }
}
