<?php

declare(strict_types=1);

namespace Tests\Smorken\LazyImport\Unit\FromHandlers;

use Illuminate\Container\Container;
use Illuminate\Events\Dispatcher;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\LazyCollection;
use Illuminate\Support\MessageBag;
use Mockery as m;
use PHPUnit\Framework\Attributes\Test;
use PHPUnit\Framework\TestCase;
use Smorken\LazyImport\Actions\Results\ImportResult;
use Smorken\LazyImport\Contracts\Actions\ImportAction;
use Smorken\LazyImport\Contracts\Repositories\SourceRepository;
use Smorken\LazyImport\FromHandlers\Handler;
use Tests\Smorken\LazyImport\Concerns\HasBarMap;
use Tests\Smorken\LazyImport\Concerns\HasFooMap;
use Tests\Smorken\LazyImport\Concerns\WithCacheManager;
use Tests\Smorken\LazyImport\Stubs\FromHandlers\ConfigStub;
use Tests\Smorken\LazyImport\Stubs\FromHandlers\FooAndBarImporter;
use Tests\Smorken\LazyImport\Stubs\FromHandlers\RelatedCollectionConverter;
use Tests\Smorken\LazyImport\Stubs\ModelStub;

class FromHandlerImporterTest extends TestCase
{
    use HasBarMap, HasFooMap, WithCacheManager;

    #[Test]
    public function it_imports_from_many_handlers(): void
    {
        $sourceRepo = m::mock(SourceRepository::class);
        $fooImportAction = m::mock(ImportAction::class);
        $barImportAction = m::mock(ImportAction::class);
        $sut = new FooAndBarImporter(
            $sourceRepo,
            new ConfigStub(
                foo: new Handler(
                    'foo',
                    $this->getFooMap(),
                    $fooImportAction,
                ),
                bar: new Handler(
                    'bar',
                    $this->getBarMap(),
                    $barImportAction,
                    null,
                    new RelatedCollectionConverter
                )
            )
        );
        $lazyCollection = new LazyCollection([
            new ModelStub(['foo_id' => 1, 'foo_name' => 'f 1']),
            new ModelStub(['foo_id' => 2, 'foo_name' => 'f 2']),
            new ModelStub(['foo_id' => 3, 'foo_name' => 'f 3']),
            new ModelStub(['foo_id' => 4, 'foo_name' => 'f 4']),
            new ModelStub(['foo_id' => 5, 'foo_name' => 'f 5']),
        ]);
        $sourceRepo->expects()
            ->__invoke(null)
            ->andReturns($lazyCollection);
        $fooImportAction->shouldReceive('__invoke')
            ->twice()
            ->withArgs(function (Collection $items) {
                $this->assertCount(2, $items);

                return count($items) === 2;
            })->andReturns(
                new ImportResult(
                    1,
                    1,
                    1,
                    0,
                ),
                new ImportResult(
                    2,
                    0,
                    1,
                    1,
                ),
            );
        $barImportAction->shouldReceive('__invoke')
            ->times(5)
            ->withArgs(function (Collection $items) {
                $this->assertCount(2, $items);

                return count($items) === 2;
            })->andReturns(
                new ImportResult(
                    1,
                    1,
                    1,
                    0,
                ),
                new ImportResult(
                    0,
                    2,
                    0,
                    0,
                ),
                new ImportResult(
                    1,
                    1,
                    0,
                    1,
                ),
                new ImportResult(
                    0,
                    2,
                    0,
                    0,
                ),
                new ImportResult( //finalize
                    1,
                    1,
                    0,
                    1,
                ),
            );
        $fooImportAction->shouldReceive('__invoke') //finalize
            ->once()
            ->withArgs(function (Collection $items) {
                $this->assertCount(1, $items);

                return count($items) === 1;
            })->andReturns(
                new ImportResult(
                    0,
                    1,
                    0,
                    0,
                ),
            );
        $fooImportAction->expects()
            ->getMessages()
            ->andReturns(new MessageBag);
        $barImportAction->expects()
            ->getMessages()
            ->andReturns(new MessageBag);
        $result = $sut->import(null, 2);
        $results = $result->results();
        $this->assertEquals([
            'foo::validated' => 5,
            'foo::total' => 5,
            'foo::existing' => 3,
            'foo::created' => 2,
            'foo::updated' => 2,
            'foo::touched' => 1,
        ], $results[0]->getCounters());
        $this->assertEquals([], $results[0]->getMessages()->toArray());
        $this->assertEquals([
            'bar::validated' => 10,
            'bar::total' => 10,
            'bar::existing' => 3,
            'bar::created' => 7,
            'bar::updated' => 1,
            'bar::touched' => 2,
        ], $results[1]->getCounters());
        $this->assertEquals([], $results[1]->getMessages()->toArray());
    }

    protected function setUp(): void
    {
        parent::setUp();
        $c = Container::getInstance();
        $dispatcher = new Dispatcher($c);
        $c->bind('events', fn () => $dispatcher);
        $c->bind('cache', fn () => $this->getCacheManager());
        Event::setFacadeApplication($c);
        $this->getCacheManager()->allows()->refreshEventDispatcher();
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        m::close();
    }
}
