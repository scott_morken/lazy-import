<?php

declare(strict_types=1);

namespace Tests\Smorken\LazyImport\Unit\Actions;

use Illuminate\Contracts\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Mockery as m;
use PHPUnit\Framework\Attributes\Test;
use PHPUnit\Framework\TestCase;
use Smorken\LazyImport\Actions\EloquentImportAction;
use Smorken\LazyImport\Contracts\Data\TargetData;
use Smorken\LazyImport\Contracts\Repositories\TargetRepository;
use Smorken\LazyImport\Maps\SourceToTargetMap;
use Smorken\LazyImport\ModelCreators\BatchEloquentCreator;
use Smorken\LazyImport\ModelTouchers\SingleEloquentToucher;
use Tests\Smorken\LazyImport\Concerns\WithMockConnection;
use Tests\Smorken\LazyImport\Stubs\ModelStub;
use Tests\Smorken\LazyImport\Stubs\TargetDataStub;

class EloquentImportActionTest extends TestCase
{
    use WithMockConnection;

    #[Test]
    public function it_imports_mapped_collection(): void
    {
        $m = new class extends Model
        {
            protected $fillable = ['id', 'name', 'description', 'stub_at'];
        };
        $model = m::mock($m)->makePartial();
        $map = new class(TargetDataStub::class) extends SourceToTargetMap
        {
            protected array|string|null $sourceIdentifierKey = 'foo_id';

            protected array|string|null $targetIdentifierKey = 'id';

            protected function getTargetAndSourceMap(): array
            {
                return [
                    'id' => 'foo_id',
                    'name' => 'foo_name',
                    'description' => fn ($model) => $model->getDescription(),
                    'stub_at' => fn () => null,
                ];
            }
        };
        $existing = new Collection([
            m::mock($m->newInstance([
                'id' => 1,
                'name' => 'f 1',
                'description' => 'some description',
                'stub_at' => null,
                'created_at' => '2023-10-10',
                'updated_at' => '2023-10-10',
            ])->syncOriginal())
                ->makePartial(),
            m::mock($m->newInstance([
                'id' => 2, 'name' => 'f 2-a', 'description' => 'some description', 'stub_at' => null,
            ])->syncOriginal())
                ->makePartial(),
            m::mock($m->newInstance([
                'id' => 4, 'name' => 'f 4-a', 'description' => 'some description', 'stub_at' => null,
            ])->syncOriginal())
                ->makePartial(),
        ]);
        $existing->each(function (Model $i) {
            $i->allows()->save()->andReturns(true);
            $i->allows()->touch()->andReturns(true);
        });
        $targetRepository = m::mock(TargetRepository::class);
        $targetRepository->shouldReceive('__invoke')
            ->withArgs(function (Collection $targetDatum) {
                $this->assertEquals([
                    [
                        'id' => 1,
                    ],
                    [
                        'id' => 2,
                    ],
                    [
                        'id' => 3,
                    ],
                    [
                        'id' => 4,
                    ],
                    [
                        'id' => 5,
                    ],
                ], $targetDatum->map(fn (TargetData $t) => $t->identifier())->toArray());

                return $targetDatum->count() === 5;
            })
            ->andReturns($existing);
        $savableModel = m::mock(Model::class);
        $savableModel->expects()->save()->twice()->andReturns(true);
        $model->expects()
            ->newInstance(['id' => 3, 'name' => 'f 3', 'description' => 'some description', 'stub_at' => null])
            ->andReturns($savableModel);
        $model->expects()
            ->newInstance(['id' => 5, 'name' => 'f 5', 'description' => 'some description', 'stub_at' => null])
            ->andReturns($savableModel);
        $mapped = new Collection([
            $map->newInstance(new ModelStub(['foo_id' => 1, 'foo_name' => 'f 1'])),
            $map->newInstance(new ModelStub(['foo_id' => 2, 'foo_name' => 'f 2'])),
            $map->newInstance(new ModelStub(['foo_id' => 3, 'foo_name' => 'f 3'])),
            $map->newInstance(new ModelStub(['foo_id' => 4, 'foo_name' => 'f 4'])),
            $map->newInstance(new ModelStub(['foo_id' => 5, 'foo_name' => 'f 5'])),
        ]);
        $sut = new class($model, $targetRepository, null, new SingleEloquentToucher($model)) extends EloquentImportAction {};
        $results = $sut($mapped);
        $this->assertEquals(3, $results->existing);
        $this->assertEquals(2, $results->created);
        $this->assertEquals(2, $results->updated);
        $this->assertEquals(1, $results->touched);
    }

    #[Test]
    public function it_imports_mapped_collection_with_batch_create(): void
    {
        $m = new class extends Model
        {
            protected $fillable = ['id', 'name', 'description', 'stub_at'];
        };
        $model = m::mock($m)->makePartial();
        $map = new class(TargetDataStub::class) extends SourceToTargetMap
        {
            protected array|string|null $sourceIdentifierKey = 'foo_id';

            protected array|string|null $targetIdentifierKey = 'id';

            protected function getTargetAndSourceMap(): array
            {
                return [
                    'id' => 'foo_id',
                    'name' => 'foo_name',
                    'description' => fn ($model) => $model->getDescription(),
                    'stub_at' => fn () => null,
                ];
            }
        };
        $existing = new Collection([
            m::mock($m->newInstance([
                'id' => 1,
                'name' => 'f 1',
                'description' => 'some description',
                'stub_at' => null,
                'created_at' => '2023-10-10',
                'updated_at' => '2023-10-10',
            ])->syncOriginal())
                ->makePartial(),
            m::mock($m->newInstance([
                'id' => 2, 'name' => 'f 2-a', 'description' => 'some description', 'stub_at' => null,
            ])->syncOriginal())
                ->makePartial(),
            m::mock($m->newInstance([
                'id' => 4, 'name' => 'f 4-a', 'description' => 'some description', 'stub_at' => null,
            ])->syncOriginal())
                ->makePartial(),
        ]);
        $existing->each(function (Model $i) {
            $i->allows()->save()->andReturns(true);
            $i->allows()->touch()->andReturns(true);
        });
        $targetRepository = m::mock(TargetRepository::class);
        $targetRepository->shouldReceive('__invoke')
            ->withArgs(function (Collection $targetDatum) {
                $this->assertEquals([
                    [
                        'id' => 1,
                    ],
                    [
                        'id' => 2,
                    ],
                    [
                        'id' => 3,
                    ],
                    [
                        'id' => 4,
                    ],
                    [
                        'id' => 5,
                    ],
                ], $targetDatum->map(fn (TargetData $t) => $t->identifier())->toArray());

                return $targetDatum->count() === 5;
            })
            ->andReturns($existing);
        $query = m::mock(Builder::class);
        $model->expects()->query()->andReturns($query);
        $query->expects()->insertOrIgnore(m::type('array'))->andReturnUsing(
            function (array $creating) {
                $expected = [
                    [
                        'id' => 3,
                        'name' => 'f 3',
                        'description' => 'some description',
                        'stub_at' => null,
                    ],
                    [
                        'id' => 5,
                        'name' => 'f 5',
                        'description' => 'some description',
                        'stub_at' => null,
                    ],
                ];
                $this->assertCount(2, $creating);
                foreach ($expected as $i => $attributes) {
                    foreach ($attributes as $k => $v) {
                        $this->assertEquals($v, $creating[$i][$k], "i: $i, k: $k");
                    }
                }

                return count($creating);
            }
        );
        $mapped = new Collection([
            $map->newInstance(new ModelStub(['foo_id' => 1, 'foo_name' => 'f 1'])),
            $map->newInstance(new ModelStub(['foo_id' => 2, 'foo_name' => 'f 2'])),
            $map->newInstance(new ModelStub(['foo_id' => 3, 'foo_name' => 'f 3'])),
            $map->newInstance(new ModelStub(['foo_id' => 4, 'foo_name' => 'f 4'])),
            $map->newInstance(new ModelStub(['foo_id' => 5, 'foo_name' => 'f 5'])),
        ]);
        $sut = new class($model, $targetRepository, new BatchEloquentCreator($model), new SingleEloquentToucher($model)) extends EloquentImportAction {};
        $results = $sut($mapped);
        $this->assertEquals(3, $results->existing);
        $this->assertEquals(2, $results->created);
        $this->assertEquals(2, $results->updated);
        $this->assertEquals(1, $results->touched);
    }

    #[Test]
    public function it_imports_mapped_collection_with_batch_create_with_mismatch_created_count(): void
    {
        $m = new class extends Model
        {
            protected $fillable = ['id', 'name', 'description', 'stub_at'];
        };
        $model = m::mock($m)->makePartial();
        $map = new class(TargetDataStub::class) extends SourceToTargetMap
        {
            protected array|string|null $sourceIdentifierKey = 'foo_id';

            protected array|string|null $targetIdentifierKey = 'id';

            protected function getTargetAndSourceMap(): array
            {
                return [
                    'id' => 'foo_id',
                    'name' => 'foo_name',
                    'description' => fn ($model) => $model->getDescription(),
                    'stub_at' => fn () => null,
                ];
            }
        };
        $existing = new Collection([
            m::mock($m->newInstance([
                'id' => 1,
                'name' => 'f 1',
                'description' => 'some description',
                'stub_at' => null,
                'created_at' => '2023-10-10',
                'updated_at' => '2023-10-10',
            ])->syncOriginal())
                ->makePartial(),
            m::mock($m->newInstance([
                'id' => 2, 'name' => 'f 2-a', 'description' => 'some description', 'stub_at' => null,
            ])->syncOriginal())
                ->makePartial(),
            m::mock($m->newInstance([
                'id' => 4, 'name' => 'f 4-a', 'description' => 'some description', 'stub_at' => null,
            ])->syncOriginal())
                ->makePartial(),
        ]);
        $existing->each(function (Model $i) {
            $i->allows()->save()->andReturns(true);
            $i->allows()->touch()->andReturns(true);
        });
        $targetRepository = m::mock(TargetRepository::class);
        $targetRepository->shouldReceive('__invoke')
            ->withArgs(function (Collection $targetDatum) {
                $this->assertEquals([
                    [
                        'id' => 1,
                    ],
                    [
                        'id' => 2,
                    ],
                    [
                        'id' => 3,
                    ],
                    [
                        'id' => 4,
                    ],
                    [
                        'id' => 5,
                    ],
                ], $targetDatum->map(fn (TargetData $t) => $t->identifier())->toArray());

                return $targetDatum->count() === 5;
            })
            ->andReturns($existing);
        $query = m::mock(Builder::class);
        $model->expects()->query()->andReturns($query);
        $query->expects()->insertOrIgnore(m::type('array'))->andReturnUsing(
            function (array $creating) {
                $expected = [
                    [
                        'id' => 3,
                        'name' => 'f 3',
                        'description' => 'some description',
                        'stub_at' => null,
                    ],
                    [
                        'id' => 5,
                        'name' => 'f 5',
                        'description' => 'some description',
                        'stub_at' => null,
                    ],
                ];
                $this->assertCount(2, $creating);
                foreach ($expected as $i => $attributes) {
                    foreach ($attributes as $k => $v) {
                        $this->assertEquals($v, $creating[$i][$k], "i: $i, k: $k");
                    }
                }

                return 1;
            }
        );
        $mapped = new Collection([
            $map->newInstance(new ModelStub(['foo_id' => 1, 'foo_name' => 'f 1'])),
            $map->newInstance(new ModelStub(['foo_id' => 2, 'foo_name' => 'f 2'])),
            $map->newInstance(new ModelStub(['foo_id' => 3, 'foo_name' => 'f 3'])),
            $map->newInstance(new ModelStub(['foo_id' => 4, 'foo_name' => 'f 4'])),
            $map->newInstance(new ModelStub(['foo_id' => 5, 'foo_name' => 'f 5'])),
        ]);
        $sut = new class($model, $targetRepository, new BatchEloquentCreator($model), new SingleEloquentToucher($model)) extends EloquentImportAction {};
        $results = $sut($mapped);
        $this->assertEquals(3, $results->existing);
        $this->assertEquals(1, $results->created);
        $this->assertEquals(2, $results->updated);
        $this->assertEquals(1, $results->touched);
        $this->assertEquals(['CreateError:batch-0' => ['Expected 2 records, created 1 records.']],
            $sut->getMessages()->toArray());
    }

    #[Test]
    public function it_imports_mapped_collection_with_batch_toucher(): void
    {
        $m = new class extends Model
        {
            protected $fillable = ['id', 'name', 'description', 'stub_at'];
        };
        $model = m::mock($m)->makePartial();
        $map = new class(TargetDataStub::class) extends SourceToTargetMap
        {
            protected array|string|null $sourceIdentifierKey = 'foo_id';

            protected array|string|null $targetIdentifierKey = 'id';

            protected function getTargetAndSourceMap(): array
            {
                return [
                    'id' => 'foo_id',
                    'name' => 'foo_name',
                    'description' => fn ($model) => $model->getDescription(),
                    'stub_at' => fn () => null,
                ];
            }
        };
        $existing = new Collection([
            m::mock($m->newInstance([
                'id' => 1,
                'name' => 'f 1',
                'description' => 'some description',
                'stub_at' => null,
                'created_at' => '2023-10-10',
                'updated_at' => '2023-10-10',
            ])->syncOriginal())
                ->makePartial(),
            m::mock($m->newInstance([
                'id' => 2, 'name' => 'f 2-a', 'description' => 'some description', 'stub_at' => null,
            ])->syncOriginal())
                ->makePartial(),
            m::mock($m->newInstance([
                'id' => 4, 'name' => 'f 4-a', 'description' => 'some description', 'stub_at' => null,
            ])->syncOriginal())
                ->makePartial(),
        ]);
        $existing->each(function (Model $i) {
            $i->allows()->save()->andReturns(true);
            $i->allows()->touch()->andReturns(true);
        });
        $targetRepository = m::mock(TargetRepository::class);
        $targetRepository->shouldReceive('__invoke')
            ->withArgs(function (Collection $targetDatum) {
                $this->assertEquals([
                    [
                        'id' => 1,
                    ],
                    [
                        'id' => 2,
                    ],
                    [
                        'id' => 3,
                    ],
                    [
                        'id' => 4,
                    ],
                    [
                        'id' => 5,
                    ],
                ], $targetDatum->map(fn (TargetData $t) => $t->identifier())->toArray());

                return $targetDatum->count() === 5;
            })
            ->andReturns($existing);
        $savableModel = m::mock(Model::class);
        $savableModel->expects()->save()->twice()->andReturns(true);
        $model->expects()
            ->newInstance(['id' => 3, 'name' => 'f 3', 'description' => 'some description', 'stub_at' => null])
            ->andReturns($savableModel);
        $model->expects()
            ->newInstance(['id' => 5, 'name' => 'f 5', 'description' => 'some description', 'stub_at' => null])
            ->andReturns($savableModel);
        $mapped = new Collection([
            $map->newInstance(new ModelStub(['foo_id' => 1, 'foo_name' => 'f 1'])),
            $map->newInstance(new ModelStub(['foo_id' => 2, 'foo_name' => 'f 2'])),
            $map->newInstance(new ModelStub(['foo_id' => 3, 'foo_name' => 'f 3'])),
            $map->newInstance(new ModelStub(['foo_id' => 4, 'foo_name' => 'f 4'])),
            $map->newInstance(new ModelStub(['foo_id' => 5, 'foo_name' => 'f 5'])),
        ]);
        $q = m::mock(Builder::class);
        $model->expects()->newQuery()->andReturns($q);
        $q->expects()->whereIn('id', [1])->andReturnSelf();
        $q->expects()->update(m::type('array'))->andReturns(1);
        $sut = new class($model, $targetRepository) extends EloquentImportAction {};
        $results = $sut($mapped);
        $this->assertEquals(3, $results->existing);
        $this->assertEquals(2, $results->created);
        $this->assertEquals(2, $results->updated);
        $this->assertEquals(1, $results->touched);
    }

    protected function setUp(): void
    {
        parent::setUp();
        $this->initConnectionResolver();
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        m::close();
    }
}
