<?php

declare(strict_types=1);

namespace Tests\Smorken\LazyImport\Unit\Actions;

use Carbon\Carbon;
use Illuminate\Contracts\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Mockery as m;
use PHPUnit\Framework\Attributes\Test;
use PHPUnit\Framework\TestCase;
use Smorken\LazyImport\Actions\EloquentCleanupAction;
use Smorken\Support\Contracts\Filter;

class EloquentCleanupActionTest extends TestCase
{
    #[Test]
    public function it_returns_count_when_filter_is_null_and_delete_is_successful(): void
    {
        Carbon::setTestNow(Carbon::parse('2023-10-12'));
        $m = m::mock(Model::class);
        $query = m::mock(Builder::class);
        $query->allows()->getModel()->andReturn($m);
        $m->expects()
            ->newQuery()
            ->andReturn($query);
        $query->expects()
            ->whereDate('updated_at', '<', '2023-09-28')
            ->andReturnSelf();
        $query->expects()
            ->delete()
            ->andReturn(10);
        $sut = new class($m) extends EloquentCleanupAction {};
        $result = $sut(null);
        $this->assertEquals(10, $result->count);
    }

    #[Test]
    public function it_returns_count_when_filter_is_valid_and_delete_is_successful(): void
    {
        $m = m::mock(Model::class);
        $query = m::mock(Builder::class);
        $query->allows()->getModel()->andReturn($m);
        $m->expects()
            ->newQuery()
            ->andReturn($query);
        $query->expects()
            ->whereDate('updated_at', '<', '2023-10-10')
            ->andReturnSelf();
        $query->expects()
            ->delete()
            ->andReturn(10);
        $sut = new class($m) extends EloquentCleanupAction {};
        $result = $sut(new \Smorken\Support\Filter(['cleanupBefore' => '2023-10-10']));
        $this->assertEquals(10, $result->count);
    }

    #[Test]
    public function it_returns_count_with_model_cleanupBefore_scope_and_delete_is_successful(): void
    {
        $filter = new \Smorken\Support\Filter(['cleanupBefore' => '2023-10-10']);
        $model = new class extends Model
        {
            public function scopeCleanupBefore(Builder $query, $date)
            {
                return $query->whereDate('updated_at', '<', $date);
            }
        };
        $m = m::mock($model)->makePartial();
        $query = m::mock(Builder::class);
        $query->allows()->getModel()->andReturn($m);
        $m->expects()
            ->newQuery()
            ->andReturn($query);
        $query->expects()
            ->cleanupBefore('2023-10-10')
            ->andReturnSelf();
        $query->expects()
            ->delete()
            ->andReturn(10);
        $sut = new class($m) extends EloquentCleanupAction {};
        $result = $sut($filter);
        $this->assertEquals(10, $result->count);
    }

    #[Test]
    public function it_returns_count_with_model_filter_scope_and_delete_is_successful(): void
    {
        $filter = new \Smorken\Support\Filter(['cleanupBefore' => '2023-10-10', 'foo' => 'bar']);
        $model = new class extends Model
        {
            public function scopeFilter(Builder $query, Filter $filter)
            {
                return $query;
            }
        };
        $m = m::mock($model)->makePartial();
        $query = m::mock(Builder::class);
        $query->allows()->getModel()->andReturn($m);
        $m->expects()
            ->newQuery()
            ->andReturn($query);
        $query->shouldReceive('filter')
            ->withArgs(function (Filter $filter) {
                $this->assertFalse($filter->has('cleanupBefore'));
                $this->assertEquals('bar', $filter->getAttribute('foo'));

                return true;
            });
        $query->expects()
            ->whereDate('updated_at', '<', '2023-10-10')
            ->andReturnSelf();
        $query->expects()
            ->delete()
            ->andReturn(10);
        $sut = new class($m) extends EloquentCleanupAction {};
        $result = $sut($filter);
        $this->assertEquals(10, $result->count);
    }

    #[Test]
    public function it_returns_zero_when_filter_is_null_by_default(): void
    {
        $m = m::mock(Model::class);
        $sut = new class($m) extends EloquentCleanupAction
        {
            protected function modifyFilter(?Filter $filter): ?Filter
            {
                return null;
            }
        };
        $result = $sut(null);
        $this->assertEquals(0, $result->count);
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        m::close();
        Carbon::setTestNow();
    }
}
