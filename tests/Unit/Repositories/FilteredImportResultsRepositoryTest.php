<?php

declare(strict_types=1);

namespace Tests\Smorken\LazyImport\Unit\Repositories;

use Illuminate\Http\Request;
use PHPUnit\Framework\Attributes\Test;
use PHPUnit\Framework\TestCase;
use Smorken\LazyImport\Models\Eloquent\ImportResult;
use Smorken\LazyImport\Repositories\FilteredImportResultsRepository;
use Smorken\QueryStringFilter\QueryStringFilter;
use Tests\Smorken\LazyImport\Concerns\WithMockConnection;

class FilteredImportResultsRepositoryTest extends TestCase
{
    use WithMockConnection;

    #[Test]
    public function it_orders_by_created_by_desc(): void
    {
        $sut = new FilteredImportResultsRepository(new ImportResult);
        $this->getMockPDO()->expects()
            ->prepare('select count(*) as aggregate from `import_results`')
            ->andReturns($this->getMockStatement());
        $this->getMockStatement()->expects()
            ->execute();
        $this->getMockStatement()->expects()
            ->fetchAll()
            ->andReturns([['aggregate' => 1]]);
        $this->getMockPDO()->expects()
            ->prepare('select * from `import_results` order by `created_at` desc limit 20 offset 0')
            ->andReturns($this->getMockStatement());
        $this->getMockStatement()->expects()
            ->execute();
        $this->getMockStatement()->expects()
            ->fetchAll()
            ->andReturns([]);
        $results = $sut(new QueryStringFilter(new Request));
        $this->assertEquals(0, $results->count());
    }

    #[Test]
    public function it_filters_by_created_date(): void
    {
        $sut = new FilteredImportResultsRepository(new ImportResult);
        $this->getMockPDO()->expects()
            ->prepare('select count(*) as aggregate from `import_results` where `created_at` >= ? and `created_at` <= ?')
            ->andReturns($this->getMockStatement());
        $this->getMockStatement()->expects()
            ->bindValue(1, '2024-01-01 00:00:00', 2);
        $this->getMockStatement()->expects()
            ->bindValue(2, '2024-01-10 00:00:00', 2);
        $this->getMockStatement()->expects()
            ->execute();
        $this->getMockStatement()->expects()
            ->fetchAll()
            ->andReturns([['aggregate' => 1]]);
        $this->getMockPDO()->expects()
            ->prepare('select * from `import_results` where `created_at` >= ? and `created_at` <= ? order by `created_at` desc limit 20 offset 0')
            ->andReturns($this->getMockStatement());
        $this->getMockStatement()->expects()
            ->bindValue(1, '2024-01-01 00:00:00', 2);
        $this->getMockStatement()->expects()
            ->bindValue(2, '2024-01-10 00:00:00', 2);
        $this->getMockStatement()->expects()
            ->execute();
        $this->getMockStatement()->expects()
            ->fetchAll()
            ->andReturns([]);
        $request = (new Request)->merge(['filter' => ['createdAfter' => '2024-01-01', 'createdBefore' => '2024-01-10']]);
        $filter = QueryStringFilter::from($request)->setFilters(['createdAfter', 'createdBefore']);
        $results = $sut($filter);
        $this->assertEquals(0, $results->count());
    }

    protected function setUp(): void
    {
        parent::setUp();
        $this->initConnectionResolver();
    }
}
