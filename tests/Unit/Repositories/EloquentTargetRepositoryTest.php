<?php

declare(strict_types=1);

namespace Tests\Smorken\LazyImport\Unit\Repositories;

use Illuminate\Contracts\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Mockery as m;
use PHPUnit\Framework\Attributes\Test;
use PHPUnit\Framework\TestCase;
use Smorken\LazyImport\Models\Concerns\HasIdentifiersOnModel;
use Smorken\LazyImport\Repositories\EloquentTargetRepository;
use Tests\Smorken\LazyImport\Concerns\WithCacheManager;
use Tests\Smorken\LazyImport\Concerns\WithMockConnection;
use Tests\Smorken\LazyImport\Stubs\TargetDataStub;

class EloquentTargetRepositoryTest extends TestCase
{
    use WithCacheManager, WithMockConnection;

    #[Test]
    public function it_creates_a_query_with_correct_identifiers(): void
    {
        $model = new class extends Model
        {
            use HasIdentifiersOnModel;

            protected $fillable = ['foo', 'bar'];

            protected $table = 't';
        };
        $this->getMockPDO()
            ->expects()
            ->prepare('select * from `t` where (`foo` = ? or `foo` = ?)')
            ->andReturn($this->getMockStatement());
        $this->getMockStatement()->expects()->bindValue(1, 1, 1)->andReturn(true);
        $this->getMockStatement()->expects()->bindValue(2, 2, 1)->andReturn(true);
        $this->getMockStatement()->expects()->execute()->andReturn(true);
        $this->getMockStatement()->expects()->fetchAll()->andReturn([
            ['id' => 1, 'foo' => 1, 'bar' => 'bar 1'],
            ['id' => 2, 'foo' => 2, 'bar' => 'bar 2'],
        ]);
        $sut = new class($model) extends EloquentTargetRepository {};
        $c = $sut(new Collection([
            new TargetDataStub(['foo' => 1], 1, 'foo 1'),
            new TargetDataStub(['foo' => 2], 2, 'foo 2'),
        ]));
        $this->assertEquals([
            ['id' => 1, 'foo' => 1, 'bar' => 'bar 1'],
            ['id' => 2, 'foo' => 2, 'bar' => 'bar 2'],
        ], $c->toArray());
    }

    #[Test]
    public function it_returns_a_collection(): void
    {
        $model = m::mock(Model::class);
        $query = m::mock(Builder::class);
        $query->allows()->getModel()->andReturn($model);
        $model->expects()
            ->newQuery()
            ->andReturn($query);
        $query->expects()
            ->identifiers([['id' => 1], ['id' => 2]])
            ->andReturnSelf();
        $collected = new Collection;
        $query->expects()
            ->get(['*'])
            ->andReturn($collected);
        $sut = new EloquentTargetRepository($model);
        $results = $sut(new Collection([
            new TargetDataStub(['id' => 1], 1, 'foo 1'),
            new TargetDataStub(['id' => 2], 2, 'foo 2'),
        ]));
        $this->assertSame($collected, $results);
    }

    protected function setUp(): void
    {
        parent::setUp();
        $this->initCacheAssist(true);
        $this->initConnectionResolver();
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        m::close();
    }
}
