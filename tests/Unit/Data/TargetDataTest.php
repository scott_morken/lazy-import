<?php

declare(strict_types=1);

namespace Tests\Smorken\LazyImport\Unit\Data;

use Carbon\Carbon;
use PHPUnit\Framework\Attributes\Test;
use PHPUnit\Framework\TestCase;
use Tests\Smorken\LazyImport\Stubs\TargetDataStub;

class TargetDataTest extends TestCase
{
    #[Test]
    public function it_creates_an_array_from_properties(): void
    {
        $sut = new TargetDataStub(
            ['id' => 1],
            1,
            'foo',
            null,
            Carbon::parse('2023-10-12 01:00:00')
        );
        $expected = [
            'id' => 1,
            'name' => 'foo',
            'description' => null,
        ];
        $test = $sut->toArray();
        foreach ($expected as $k => $v) {
            $this->assertEquals($v, $test[$k]);
        }
        $this->assertEquals('2023-10-12 01:00:00', (string) $test['stub_at']);
    }
}
