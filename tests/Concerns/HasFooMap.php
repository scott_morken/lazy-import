<?php

declare(strict_types=1);

namespace Tests\Smorken\LazyImport\Concerns;

use Smorken\LazyImport\Contracts\SourceToTargetMap;
use Tests\Smorken\LazyImport\Stubs\TargetDataStub;

trait HasFooMap
{
    protected function getFooMap(string $targetData = TargetDataStub::class): SourceToTargetMap
    {
        return new class($targetData) extends \Smorken\LazyImport\Maps\SourceToTargetMap
        {
            protected array|string|null $sourceIdentifierKey = 'foo_id';

            protected array|string|null $targetIdentifierKey = 'id';

            protected function getTargetAndSourceMap(): array
            {
                return [
                    'id' => 'foo_id',
                    'name' => 'foo_name',
                    'description' => fn ($model) => $model->getDescription(),
                    'stub_at' => fn () => null,
                ];
            }
        };
    }
}
