<?php

declare(strict_types=1);

namespace Tests\Smorken\LazyImport\Concerns;

use Smorken\LazyImport\Contracts\SourceToTargetMap;
use Tests\Smorken\LazyImport\Stubs\TargetDataStub;

trait HasBarMap
{
    protected function getBarMap(string $targetData = TargetDataStub::class): SourceToTargetMap
    {
        return new class($targetData) extends \Smorken\LazyImport\Maps\SourceToTargetMap
        {
            protected array|string|null $sourceIdentifierKey = 'bar_id';

            protected array|string|null $targetIdentifierKey = 'id';

            protected function getTargetAndSourceMap(): array
            {
                return [
                    'id' => 'bar_id',
                    'name' => 'bar_name',
                ];
            }
        };
    }
}
