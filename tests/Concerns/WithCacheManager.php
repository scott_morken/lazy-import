<?php

namespace Tests\Smorken\LazyImport\Concerns;

use Illuminate\Cache\CacheManager;
use Illuminate\Config\Repository;
use Illuminate\Contracts\Container\Container;
use Mockery as m;
use Smorken\CacheAssist\CacheAssist;

trait WithCacheManager
{
    protected ?CacheManager $cacheManager = null;

    protected function getAppForCacheManager(array $config): Container
    {
        $app = \Illuminate\Container\Container::getInstance();
        $app->singleton('config', fn () => new Repository($config));

        return $app;
    }

    protected function getArrayCacheManager(): CacheManager
    {
        if (! $this->cacheManager) {
            $config = [
                'cache' => [
                    'default' => 'test',
                    'stores' => [
                        'test' => [
                            'driver' => 'array',
                        ],
                    ],
                ],
            ];
            $this->cacheManager = new CacheManager($this->getAppForCacheManager($config));
        }

        return $this->cacheManager;
    }

    protected function getCacheManager(): CacheManager
    {
        if (! $this->cacheManager) {
            $this->cacheManager = m::mock(CacheManager::class);
        }

        return $this->cacheManager;
    }

    protected function initCacheAssist(bool $useArray = false): void
    {
        if ($useArray) {
            $this->setCacheManagerForCacheAssist($this->getArrayCacheManager());
        } else {
            $this->setCacheManagerForCacheAssist($this->getCacheManager());
        }
    }

    protected function setCacheManagerForCacheAssist(CacheManager $cacheManager): void
    {
        CacheAssist::setCache($cacheManager);
    }
}
