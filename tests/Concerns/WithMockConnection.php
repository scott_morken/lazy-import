<?php

declare(strict_types=1);

namespace Tests\Smorken\LazyImport\Concerns;

use Illuminate\Database\ConnectionInterface;
use Illuminate\Database\ConnectionResolver;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\MySqlConnection;
use Mockery as m;
use Tests\Smorken\LazyImport\Stubs\MockPDO;

trait WithMockConnection
{
    protected \MockPDO|m\MockInterface|null $pdo = null;

    protected \PDOStatement|m\MockInterface|null $statement = null;

    protected function getMockConnection(
        string $connectionClass,
        array $config = [],
        array $methods = []
    ): ConnectionInterface {
        $this->statement = null;
        $this->getMockStatement();
        $pdo = $this->getMockPDO();
        $connection = $this->getMockBuilder($connectionClass)
            ->onlyMethods($methods)
            ->setConstructorArgs([$pdo, '', '', $config])
            ->getMock();
        $connection->enableQueryLog();

        return $connection;
    }

    protected function getMockPDO(): MockPDO
    {
        if (! $this->pdo) {
            $this->pdo = m::mock(MockPDO::class);
        }

        return $this->pdo;
    }

    protected function getMockStatement(): \PDOStatement
    {
        if (! $this->statement) {
            $this->statement = m::mock(\PDOStatement::class);
            $this->statement->shouldReceive('setFetchMode');
        }

        return $this->statement;
    }

    protected function initConnectionResolver(
        string $name = 'db',
        string $connectionClass = MySqlConnection::class
    ): void {
        $cr = new ConnectionResolver([$name => $this->getMockConnection($connectionClass)]);
        $cr->setDefaultConnection($name);
        Model::setConnectionResolver($cr);
    }
}
