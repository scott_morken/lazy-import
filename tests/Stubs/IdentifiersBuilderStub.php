<?php

declare(strict_types=1);

namespace Tests\Smorken\LazyImport\Stubs;

use Smorken\LazyImport\Models\Concerns\HasIdentifiersOnBuilder;
use Smorken\Model\QueryBuilders\Builder;

class IdentifiersBuilderStub extends Builder
{
    use HasIdentifiersOnBuilder;
}
