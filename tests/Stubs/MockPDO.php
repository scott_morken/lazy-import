<?php

declare(strict_types=1);

namespace Tests\Smorken\LazyImport\Stubs;

class MockPDO extends \PDO
{
    public function __construct() {}
}
