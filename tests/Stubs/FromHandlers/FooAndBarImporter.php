<?php

declare(strict_types=1);

namespace Tests\Smorken\LazyImport\Stubs\FromHandlers;

use Smorken\LazyImport\FromHandlers\FromHandlerImporter;

class FooAndBarImporter extends FromHandlerImporter {}
