<?php

declare(strict_types=1);

namespace Tests\Smorken\LazyImport\Stubs\FromHandlers;

use Smorken\LazyImport\Contracts\Converters\IteratorConverter;

class RelatedCollectionConverter implements IteratorConverter
{
    /**
     * @param  \Tests\Smorken\LazyImport\Stubs\ModelStub  $itemFromIterator
     */
    public function convert(mixed $itemFromIterator): \Iterator
    {
        return $itemFromIterator->many()->getIterator();
    }
}
