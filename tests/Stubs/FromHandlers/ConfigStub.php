<?php

declare(strict_types=1);

namespace Tests\Smorken\LazyImport\Stubs\FromHandlers;

use Smorken\LazyImport\Contracts\FromHandlers\Handler;
use Smorken\LazyImport\FromHandlers\FromHandlerConfig;

class ConfigStub extends FromHandlerConfig
{
    public function __construct(
        public Handler $foo,
        public Handler $bar
    ) {}
}
