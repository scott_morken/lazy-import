<?php

declare(strict_types=1);

namespace Tests\Smorken\LazyImport\Stubs;

use Illuminate\Support\Collection;
use Smorken\Model\Eloquent;

class ModelStub extends Eloquent
{
    protected $fillable = ['foo_id', 'foo_name'];

    public function getDescription(): string
    {
        return 'some description';
    }

    public function many(): Collection
    {
        return new Collection([
            new RelatedStub(['bar_id' => $this->foo_id.'-a', 'bar_name' => $this->foo_name.'-a']),
            new RelatedStub(['bar_id' => $this->foo_id.'-b', 'bar_name' => $this->foo_name.'-b']),
        ]);
    }
}
