<?php

declare(strict_types=1);

namespace Tests\Smorken\LazyImport\Stubs;

use Carbon\Carbon;
use Smorken\LazyImport\Data\BaseTargetData;

class TargetDataStub extends BaseTargetData
{
    public function __construct(
        array $identifier,
        public int $id,
        public string $name,
        public ?string $description = null,
        public ?Carbon $stub_at = null,
    ) {
        parent::__construct($identifier);
    }
}
