<?php

declare(strict_types=1);

namespace Tests\Smorken\LazyImport\Stubs;

use Smorken\Model\Eloquent;

class RelatedStub extends Eloquent
{
    protected $fillable = ['bar_id', 'bar_name'];
}
