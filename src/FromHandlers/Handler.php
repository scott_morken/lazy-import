<?php

declare(strict_types=1);

namespace Smorken\LazyImport\FromHandlers;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Event;
use Smorken\LazyImport\Constants\CounterKey;
use Smorken\LazyImport\Constants\ErrorKey;
use Smorken\LazyImport\Contracts\Actions\CleanupAction;
use Smorken\LazyImport\Contracts\Actions\ImportAction;
use Smorken\LazyImport\Contracts\Actions\Results\ImportResult;
use Smorken\LazyImport\Contracts\Converters\IteratorConverter;
use Smorken\LazyImport\Contracts\LookupCache;
use Smorken\LazyImport\Contracts\Result;
use Smorken\LazyImport\Contracts\SourceToTargetMap;
use Smorken\LazyImport\Converters\ArrayIteratorConverter;
use Smorken\LazyImport\Events\ResultAvailable;
use Smorken\LazyImport\Lookups\ArrayLookupCache;
use Smorken\LazyImport\Support\IsA;
use Smorken\Support\Contracts\Filter;

class Handler implements \Smorken\LazyImport\Contracts\FromHandlers\Handler
{
    protected Collection $currentMapped;

    protected IteratorConverter $iteratorConverter;

    protected LookupCache $lookupCache;

    protected Result $result;

    public function __construct(
        protected string $key,
        protected SourceToTargetMap $sourceToTargetMap,
        protected ImportAction $importAction,
        protected ?CleanupAction $cleanupAction = null,
        ?IteratorConverter $iteratorConverter = null,
        ?LookupCache $lookupCache = null,
        ?Result $result = null,
    ) {
        $this->currentMapped = new Collection;
        if (! $result) {
            $result = new \Smorken\LazyImport\Result($key);
        }
        $this->result = $result;
        if (! $lookupCache) {
            $lookupCache = new ArrayLookupCache;
        }
        $this->lookupCache = $lookupCache;
        if (! $iteratorConverter) {
            $iteratorConverter = new ArrayIteratorConverter;
        }
        $this->iteratorConverter = $iteratorConverter;
        $this->init();
    }

    public function cleanup(?Filter $filter): void
    {
        if ($this->cleanupAction) {
            $this->increment(CounterKey::CLEANUP, ($this->cleanupAction)($filter)->count);
            // @phpstan-ignore argument.type
            $this->getResult()->getMessages()->merge($this->cleanupAction->getMessages());
        }
    }

    public function dispatchResult(): void
    {
        Event::dispatch(new ResultAvailable($this->getResult()));
    }

    public function finalize(?Filter $filter, int $count = 200): Result
    {
        $this->importCurrentItems($count, true);
        // @phpstan-ignore argument.type
        $this->getResult()->getMessages()->merge($this->importAction->getMessages());
        $this->cleanup($filter);
        $this->getResult()->stop();

        return $this->getResult();
    }

    public function getResult(): Result
    {
        return $this->result->ensure();
    }

    public function import(mixed $itemFromIterator, int $count = 200): void
    {
        $this->importCurrentItems($count);
        $this->executeMappingCycle($this->iteratorConverter->convert($itemFromIterator));
    }

    public function init(): void
    {
        $this->getResult()->start();
    }

    public function reset(): void
    {
        $this->result->resetAll();
        $this->lookupCache->reset();
    }

    public function sample(mixed $itemFromIterator): Collection
    {
        $this->currentMapped = new Collection;
        $this->executeMappingCycle($this->iteratorConverter->convert($itemFromIterator));

        return $this->currentMapped;
    }

    protected function addImportResultToResults(ImportResult $importResult): void
    {
        $this->increment(CounterKey::EXISTING, $importResult->existing);
        $this->increment(CounterKey::CREATED, $importResult->created);
        $this->increment(CounterKey::UPDATED, $importResult->updated);
        $this->increment(CounterKey::TOUCHED, $importResult->touched);
    }

    protected function continueLazyIteration(SourceToTargetMap $map, mixed $item, int $currentCount): bool
    {
        if ($this->lookupCache->has($map->identifier())) {
            return true;
        }
        if (! $map->validSource()) {
            $this->increment(CounterKey::TOTAL);
            if ($map->addInvalidSourceMessage()) {
                $this->getResult()
                    ->addMessage(
                        ErrorKey::VALIDATION,
                        $this->messageWithItem('Invalid source ['.$currentCount.'] '.$map->identifier(), $item)
                    );
            }

            return true;
        }

        return false;
    }

    protected function executeMappingCycle(\Iterator $iterator): void
    {
        $count = $this->currentMapped->count();
        while ($iterator->valid()) {
            $item = $iterator->current();
            if ($item) {
                $m = $this->sourceToTargetMap->newInstance($item);
                if ($this->continueLazyIteration($m, $item, $count)) {
                    $iterator->next();
                    $count++;

                    continue;
                }
                $this->lookupCache->add($m->identifier());
                $this->currentMapped->push($m);
                $this->increment(CounterKey::VALIDATED);
                $this->increment(CounterKey::TOTAL);
            }
            $count++;
            $iterator->next();
        }
    }

    protected function importCurrentItems(int $count, bool $final = false): void
    {
        if ($final || $this->currentMapped->count() >= $count) {
            if ($this->currentMapped->isEmpty()) {
                return;
            }
            $mapped = $this->currentMapped;
            $this->preImport($mapped);
            $importResult = ($this->importAction)($mapped);
            $this->postImport($importResult, $mapped);
            $this->addImportResultToResults($importResult);
            $this->currentMapped = new Collection;
        }
    }

    protected function increment(CounterKey|string $key, int $count = 1): void
    {
        $this->getResult()->increment($key, $count);
    }

    protected function itemToString(mixed $item): ?string
    {
        if (is_array($item)) {
            return implode(', ', $item);
        }
        if (IsA::stringable($item)) {
            return (string) $item;
        }

        return null;
    }

    protected function messageWithItem(string $message, mixed $item): string
    {
        $str = $this->itemToString($item);
        if ($str === null) {
            return $message;
        }

        return $message.' ('.$this->itemToString($item).')';
    }

    protected function postImport(ImportResult $result, Collection $mapped): void
    {
        //
    }

    protected function preImport(Collection $mapped): void
    {
        //
    }
}
