<?php

declare(strict_types=1);

namespace Smorken\LazyImport\FromHandlers;

use Illuminate\Support\Collection;
use Illuminate\Support\LazyCollection;
use Smorken\LazyImport\Contracts\FromHandlers\CombinedResult;
use Smorken\LazyImport\Contracts\FromHandlers\Config;
use Smorken\LazyImport\Contracts\FromHandlers\Importer;
use Smorken\LazyImport\Contracts\Repositories\SourceRepository;
use Smorken\Support\Contracts\Filter;

/**
 * @template TSource of SourceRepository
 * @template TConfig of \Smorken\LazyImport\Contracts\FromHandlers\Config
 */
abstract class FromHandlerImporter implements Importer
{
    protected CombinedResult $result;

    /**
     * @param  TSource  $sourceRepository
     * @param  TConfig  $config
     */
    public function __construct(protected SourceRepository $sourceRepository, protected Config $config)
    {
        $this->result = new \Smorken\LazyImport\FromHandlers\CombinedResult;
    }

    public function getResult(): CombinedResult
    {
        return $this->result->ensure();
    }

    public function import(?Filter $filter = null, int $perCycle = 200): CombinedResult
    {
        $this->getResult();
        $this->beforeCreatingMainIterator($filter, $perCycle);
        $lazyCollection = $this->getLazyCollection($filter);
        /** @var \Iterator $iterator */
        $iterator = $lazyCollection->getIterator();
        while ($iterator->valid()) {
            $this->importIterator($iterator, $perCycle);
        }

        $this->finalizeHandlers($filter, $perCycle);
        $this->addHandlerResults();
        $this->dispatchHandlerResults();

        return $this->getResult();
    }

    public function reset(): void
    {
        foreach ($this->config->getHandlers() as $handler) {
            $handler->reset();
        }
    }

    public function sample(?Filter $filter = null, int $sampleSize = 5): Collection
    {
        $this->getResult();
        $lazyCollection = $this->getLazyCollection($filter);
        /** @var \Iterator $iterator */
        $iterator = $lazyCollection->getIterator();
        $samples = $this->sampleIterator($iterator, $sampleSize);
        $this->addHandlerResults();

        return $samples;
    }

    protected function addHandlerResults(): void
    {
        foreach ($this->config->getHandlers() as $handler) {
            $this->getResult()->add($handler->getResult());
        }
    }

    /**
     * @param  \Smorken\LazyImport\Contracts\FromHandlers\Handler[]  $handlers
     */
    protected function applyImportToItem(mixed $item, array $handlers, int $perCycle): void
    {
        if (! $item) {
            return;
        }
        foreach ($handlers as $handler) {
            $handler->import($item, $perCycle);
        }
    }

    protected function applySampleToItem(mixed $item, array $handlers): Collection
    {
        $samples = new Collection;
        if (! $item) {
            return $samples;
        }
        foreach ($handlers as $handler) {
            $samples = $samples->merge($handler->sample($item));
        }

        return $samples;
    }

    protected function beforeCreatingMainIterator(?Filter $filter, int $perCycle): void
    {
        // override if needed
    }

    protected function dispatchHandlerResults(): void
    {
        foreach ($this->config->getHandlers() as $handler) {
            $handler->dispatchResult();
        }
    }

    protected function finalizeHandlers(?Filter $filter, int $perCycle): void
    {
        foreach ($this->config->getHandlers() as $handler) {
            $handler->finalize($filter, $perCycle);
        }
    }

    protected function getLazyCollection(?Filter $filter): LazyCollection
    {
        return ($this->sourceRepository)($filter);
    }

    protected function importIterator(\Iterator $iterator, int $perCycle): void
    {
        $count = 0;
        $handlers = $this->config->getHandlers();
        while ($count < $perCycle) {
            $item = $iterator->current();
            if (! $iterator->valid()) {
                break;
            }
            $this->applyImportToItem($item, $handlers, $perCycle);
            $iterator->next();
            $count++;
        }
    }

    protected function sampleIterator(\Iterator $iterator, int $sampleSize): Collection
    {
        $count = 0;
        $samples = new Collection;
        $handlers = $this->config->getHandlers();
        while ($count < $sampleSize) {
            $item = $iterator->current();
            if (! $iterator->valid()) {
                break;
            }
            $samples = $samples->merge($this->applySampleToItem($item, $handlers));
            $iterator->next();
            $count++;
        }

        return $samples;
    }
}
