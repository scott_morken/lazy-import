<?php

declare(strict_types=1);

namespace Smorken\LazyImport\FromHandlers;

use ReflectionClass;
use ReflectionProperty;
use Smorken\LazyImport\Contracts\FromHandlers\Config;
use Smorken\LazyImport\Contracts\FromHandlers\Handler;

abstract class FromHandlerConfig implements Config
{
    protected array $cachedHandlers = [];

    public function getHandlers(): array
    {
        if ($this->cachedHandlers) {
            return $this->cachedHandlers;
        }
        $rc = new ReflectionClass($this);

        $this->cachedHandlers = array_map(
            fn (ReflectionProperty $p): Handler => $this->{$p->getName()},
            $rc->getProperties(ReflectionProperty::IS_PUBLIC)
        );

        return $this->cachedHandlers;
    }
}
