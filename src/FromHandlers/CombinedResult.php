<?php

declare(strict_types=1);

namespace Smorken\LazyImport\FromHandlers;

use Carbon\Carbon;
use Smorken\LazyImport\Contracts\Result;

class CombinedResult implements \Smorken\LazyImport\Contracts\FromHandlers\CombinedResult
{
    protected Carbon $end;

    /**
     * @var Result[]
     */
    protected array $results = [];

    protected Carbon $start;

    protected bool $started = false;

    protected bool $stopped = false;

    public function add(Result $result): void
    {
        $this->results[] = $result;
    }

    public function ensure(): static
    {
        $this->start();

        return $this;
    }

    public function results(): array
    {
        return $this->results;
    }

    public function start(): void
    {
        if (! $this->started) {
            $this->start = Carbon::now();
            $this->started = true;
        }
    }

    public function stop(): void
    {
        if (! $this->stopped) {
            $this->end = Carbon::now();
            $this->stopped = true;
        }
    }

    public function toArray(): array
    {
        $this->start();
        $this->stop();
        $arr = [
            'start' => $this->start,
            'end' => $this->end,
        ];
        foreach ($this->results as $result) {
            $arr[$result->getName()] = $result->toArray();
        }

        return $arr;
    }
}
