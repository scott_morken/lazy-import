<?php

declare(strict_types=1);

namespace Smorken\LazyImport\Maps;

use Smorken\LazyImport\Contracts\Data\TargetData;
use Smorken\LazyImport\Exceptions\LazyImportException;
use Smorken\Model\Contracts\Model;

/**
 * @template TTargetData of TargetData
 */
abstract class SourceToTargetMap implements \Smorken\LazyImport\Contracts\SourceToTargetMap
{
    protected bool $addInvalidSourceMessage = true;

    protected bool $allowEmptyStringInIdentifier = false;

    protected array $cache = [
        'identifier' => null,
        'map' => null,
    ];

    protected array|string|null $sourceIdentifierKey = null;

    protected array|string|null $targetIdentifierKey = null;

    /**
     * @param  class-string<TTargetData>  $targetDataClass
     */
    public function __construct(
        protected string $targetDataClass,
        protected mixed $sourceModel = null,
    ) {}

    abstract protected function getTargetAndSourceMap(): array;

    public function addInvalidSourceMessage(): bool
    {
        return $this->addInvalidSourceMessage;
    }

    public function identifier(bool $toString = true): string|array
    {
        $identifier = $this->fromCache('identifier');
        if ($identifier === null) {
            $identifier = $this->toCache('identifier', $this->getSourceIdentifier());
        }
        if ($toString) {
            return implode(':', $identifier);
        }

        return $identifier;
    }

    public function map(): array
    {
        $cached = $this->fromCache('map');
        if ($cached) {
            return $cached;
        }
        $mapped = ['identifier' => $this->identifier(false)];
        foreach ($this->getTargetAndSourceMap() as $targetKey => $sourceKey) {
            $mapped[$targetKey] = $this->getSourceValue($sourceKey);
        }

        return $this->toCache('map', $mapped);
    }

    public function newInstance(mixed $model): static
    {
        return new static($this->targetDataClass, $this->modifyIncomingSourceModel($model));
    }

    public function sourceModel(): mixed
    {
        return $this->sourceModel;
    }

    /**
     * @return TTargetData
     */
    public function toTargetData(): TargetData
    {
        return new $this->targetDataClass(...$this->map());
    }

    public function validSource(): bool
    {
        $valid = false;
        foreach ($this->identifier(false) as $v) {
            $valid = true;
            if (! $this->identifierValueIsValid($v)) {
                return false;
            }
        }

        return $valid;
    }

    protected function fromCache(string $key): mixed
    {
        return $this->cache[$key] ?? null;
    }

    protected function getSourceIdentifier(): array
    {
        if (! $this->getSourceIdentifierKey()) {
            throw LazyImportException::noSourceIdentifier();
        }
        if (is_array($this->getSourceIdentifierKey())) {
            $parts = [];
            foreach ($this->getSourceIdentifierKey() as $i => $key) {
                $parts[$this->getTargetIdentifierKeyForSourceIdentifierKey($key, $i)] = $this->getSourceValue($key);
            }

            return $parts;
        }

        return [
            $this->getTargetIdentifierKeyForSourceIdentifierKey($this->getSourceIdentifierKey(),
                0) => $this->getSourceValue($this->getSourceIdentifierKey()),
        ];
    }

    protected function getSourceIdentifierKey(): array|string|null
    {
        return $this->sourceIdentifierKey;
    }

    protected function getSourceValue(string|\Closure $key): mixed
    {
        if (is_a($key, \Closure::class)) {
            return $key($this->sourceModel);
        }
        if (is_array($this->sourceModel)) {
            return $this->sourceModel[$key] ?? null;
        }
        if (is_a($this->sourceModel, Model::class)) {
            return $this->sourceModel->getAttribute($key);
        }

        return $this->sourceModel?->{$key};
    }

    protected function getTargetIdentifierKey(): array|string|null
    {
        return $this->targetIdentifierKey;
    }

    protected function getTargetIdentifierKeyForSourceIdentifierKey(string|\Closure $key, int $index): string
    {
        if (! $this->getTargetIdentifierKey()) {
            return $key;
        }
        if (is_string($this->getTargetIdentifierKey())) {
            return $this->getTargetIdentifierKey();
        }

        return $this->getTargetIdentifierKey()[$index] ?? $key;
    }

    protected function identifierValueIsValid(mixed $value): bool
    {
        if ($value === null) {
            return false;
        }
        if (! $this->allowEmptyStringInIdentifier && $value === '') {
            return false;
        }

        return true;
    }

    protected function modifyIncomingSourceModel(mixed $model): mixed
    {
        return $model;
    }

    protected function toCache(string $key, mixed $value): mixed
    {
        $this->cache[$key] = $value;

        return $value;
    }
}
