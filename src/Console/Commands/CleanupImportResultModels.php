<?php

declare(strict_types=1);

namespace Smorken\LazyImport\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Smorken\LazyImport\Contracts\Actions\DeleteImportResultsAction;

class CleanupImportResultModels extends Command
{
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Cleanup import results.';

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:cleanup-results {--days=60}';

    public function __construct(protected DeleteImportResultsAction $deleteImportResultsAction)
    {
        parent::__construct();
    }

    public function handle(): int
    {
        $olderThan = $this->getDaysOlderThan();
        $this->line('Deleting import results older than '.$olderThan.'...');
        $count = ($this->deleteImportResultsAction)($olderThan);
        $this->line('Deleted '.$count.' records');

        return Command::SUCCESS;
    }

    protected function getDaysOlderThan(): Carbon
    {
        $days = $this->option('days');

        return is_numeric($days) ? Carbon::now()->subDays((int) $days) : Carbon::parse($days);
    }
}
