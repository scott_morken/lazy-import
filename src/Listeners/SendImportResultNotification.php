<?php

declare(strict_types=1);

namespace Smorken\LazyImport\Listeners;

use Illuminate\Contracts\Mail\Mailer;
use Illuminate\Contracts\Queue\ShouldQueue;
use Smorken\LazyImport\Events\ResultAvailable;
use Smorken\LazyImport\Mailables\ImportResultMailable;

class SendImportResultNotification implements ShouldQueue
{
    public function __construct(protected Mailer $mailer, protected string $to, protected ?string $from) {}

    public function handle(ResultAvailable $resultAvailable): void
    {
        $this->mailer->to($this->to)->send(new ImportResultMailable($resultAvailable->result, $this->from));
    }
}
