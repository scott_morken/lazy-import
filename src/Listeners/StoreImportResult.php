<?php

declare(strict_types=1);

namespace Smorken\LazyImport\Listeners;

use Illuminate\Contracts\Queue\ShouldQueue;
use Smorken\LazyImport\Contracts\Actions\StoreImportResultsAction;
use Smorken\LazyImport\Events\ResultAvailable;

class StoreImportResult implements ShouldQueue
{
    public function __construct(protected StoreImportResultsAction $storeAction) {}

    public function handle(ResultAvailable $resultAvailable): void
    {
        ($this->storeAction)($resultAvailable->result);
    }
}
