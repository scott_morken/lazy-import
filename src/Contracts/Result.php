<?php

declare(strict_types=1);

namespace Smorken\LazyImport\Contracts;

use Carbon\Carbon;
use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Contracts\Support\MessageBag;
use Smorken\LazyImport\Constants\CounterKey;
use Smorken\LazyImport\Constants\ErrorKey;

interface Result extends Arrayable
{
    public function addMessage(ErrorKey|string $key, string $message): void;

    public function decrement(CounterKey|string $key, int $count = 1): void;

    public function ensure(): self;

    public function getCounter(CounterKey|string $key): int;

    public function getCounters(): array;

    public function getEnd(): ?Carbon;

    public function getMemoryUse(): float;

    public function getMessages(): MessageBag;

    public function getName(): string;

    public function getStart(): ?Carbon;

    public function increment(CounterKey|string $key, int $count = 1): void;

    public function initCounters(array $counters = []): void;

    public function reset(CounterKey|string $key): void;

    public function resetAll(): void;

    public function setCounter(CounterKey|string $key, int $value): void;

    /**
     * @param  array<string, int>  $counters
     */
    public function setCounters(array $counters): void;

    public function start(): void;

    public function stop(): void;
}
