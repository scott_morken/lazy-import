<?php

declare(strict_types=1);

namespace Smorken\LazyImport\Contracts\FromHandlers;

use Illuminate\Contracts\Support\Arrayable;
use Smorken\LazyImport\Contracts\Result;

interface CombinedResult extends Arrayable
{
    public function add(Result $result): void;

    public function ensure(): self;

    /**
     * @return Result[]
     */
    public function results(): array;

    public function start(): void;

    public function stop(): void;
}
