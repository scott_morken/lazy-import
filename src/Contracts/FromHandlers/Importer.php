<?php

declare(strict_types=1);

namespace Smorken\LazyImport\Contracts\FromHandlers;

use Illuminate\Support\Collection;
use Smorken\Support\Contracts\Filter;

interface Importer
{
    public function getResult(): CombinedResult;

    public function import(?Filter $filter = null, int $perCycle = 200): CombinedResult;

    public function reset(): void;

    public function sample(?Filter $filter = null, int $sampleSize = 5): Collection;
}
