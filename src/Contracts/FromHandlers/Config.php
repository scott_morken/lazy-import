<?php

declare(strict_types=1);

namespace Smorken\LazyImport\Contracts\FromHandlers;

interface Config
{
    /**
     * @return \Smorken\LazyImport\Contracts\FromHandlers\Handler[]
     */
    public function getHandlers(): array;
}
