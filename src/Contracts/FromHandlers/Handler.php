<?php

declare(strict_types=1);

namespace Smorken\LazyImport\Contracts\FromHandlers;

use Illuminate\Support\Collection;
use Smorken\LazyImport\Contracts\Result;
use Smorken\Support\Contracts\Filter;

interface Handler
{
    public function cleanup(?Filter $filter): void;

    public function dispatchResult(): void;

    public function finalize(?Filter $filter, int $count = 200): Result;

    public function getResult(): Result;

    public function import(mixed $itemFromIterator, int $count = 200): void;

    public function sample(mixed $itemFromIterator): Collection;

    public function reset(): void;
}
