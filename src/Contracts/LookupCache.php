<?php

declare(strict_types=1);

namespace Smorken\LazyImport\Contracts;

interface LookupCache
{
    public function has(string $key): bool;

    public function add(string $key): void;

    public function reset(): void;
}
