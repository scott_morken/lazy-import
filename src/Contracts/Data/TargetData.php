<?php

declare(strict_types=1);

namespace Smorken\LazyImport\Contracts\Data;

use Illuminate\Contracts\Support\Arrayable;

interface TargetData extends Arrayable
{
    public function createAttributes(): array;

    public function identifier(): array;

    public function updateAttributes(): array;
}
