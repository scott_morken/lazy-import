<?php

namespace Smorken\LazyImport\Contracts\Models;

use Smorken\Model\Contracts\Model;

/**
 * @property int $id
 * @property string $importer
 * @property array $data
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @phpstan-require-extends \Smorken\LazyImport\Models\Eloquent\ImportResult
 */
interface ImportResult extends Model {}
