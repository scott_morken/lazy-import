<?php

declare(strict_types=1);

namespace Smorken\LazyImport\Contracts\Repositories;

use Smorken\Domain\Repositories\Contracts\LazyCollectionRepository;

interface SourceRepository extends LazyCollectionRepository {}
