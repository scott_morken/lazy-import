<?php

declare(strict_types=1);

namespace Smorken\LazyImport\Contracts\Repositories;

use Illuminate\Support\Collection;
use Smorken\Domain\Repositories\Contracts\Repository;

interface TargetRepository extends Repository
{
    public function __invoke(Collection $targetDatum): Collection;
}
