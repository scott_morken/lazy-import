<?php

declare(strict_types=1);

namespace Smorken\LazyImport\Contracts\Actions;

use Illuminate\Support\Collection;
use Smorken\Domain\Actions\Contracts\Action;
use Smorken\LazyImport\Contracts\Actions\Results\ImportResult;
use Smorken\LazyImport\Contracts\Repositories\TargetRepository;

interface ImportAction extends Action
{
    /**
     * @param  \Illuminate\Support\Collection<int, \Smorken\LazyImport\Contracts\SourceToTargetMap>  $mapped
     */
    public function __invoke(Collection $mapped): ImportResult;

    public function getTargetRepository(): TargetRepository;
}
