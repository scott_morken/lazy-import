<?php

declare(strict_types=1);

namespace Smorken\LazyImport\Contracts\Actions;

use Smorken\Domain\Actions\Contracts\Action;
use Smorken\LazyImport\Contracts\Models\ImportResult;
use Smorken\LazyImport\Contracts\Result;

interface StoreImportResultsAction extends Action
{
    public function __invoke(Result $result): ImportResult;
}
