<?php

declare(strict_types=1);

namespace Smorken\LazyImport\Contracts\Actions;

use Carbon\Carbon;
use Smorken\Domain\Actions\Contracts\Action;

interface DeleteImportResultsAction extends Action
{
    public function __invoke(Carbon|string $olderThan = '-60 days'): int;
}
