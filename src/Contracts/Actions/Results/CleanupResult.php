<?php

declare(strict_types=1);

namespace Smorken\LazyImport\Contracts\Actions\Results;

use Smorken\Domain\Actions\Contracts\Results\Result;

/**
 * @property positive-int $count
 *
 * @phpstan-require-extends \Smorken\LazyImport\Actions\Results\CleanupResult
 */
interface CleanupResult extends Result {}
