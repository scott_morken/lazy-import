<?php

declare(strict_types=1);

namespace Smorken\LazyImport\Contracts\Actions\Results;

use Smorken\Domain\Actions\Contracts\Results\Result;

/**
 * @property int $existing
 * @property int $created
 * @property int $updated
 * @property int $touched
 *
 * @phpstan-require-extends \Smorken\LazyImport\Actions\Results\ImportResult
 */
interface ImportResult extends Result {}
