<?php

declare(strict_types=1);

namespace Smorken\LazyImport\Contracts\Actions;

use Smorken\Domain\Actions\Contracts\Action;
use Smorken\LazyImport\Contracts\Actions\Results\CleanupResult;
use Smorken\Support\Contracts\Filter;

interface CleanupAction extends Action
{
    public function __invoke(?Filter $filter): CleanupResult;
}
