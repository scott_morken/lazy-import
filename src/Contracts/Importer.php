<?php

declare(strict_types=1);

namespace Smorken\LazyImport\Contracts;

use Illuminate\Support\Collection;
use Smorken\LazyImport\Contracts\Actions\CleanupAction;
use Smorken\LazyImport\Contracts\Actions\ImportAction;
use Smorken\LazyImport\Contracts\Repositories\SourceRepository;
use Smorken\Support\Contracts\Filter;

interface Importer
{
    public function cleanup(?Filter $filter = null): Result;

    public function dispatchResults(): void;

    public function getCleanupAction(): ?CleanupAction;

    public function getImportAction(): ImportAction;

    public function getLookupCache(): LookupCache;

    public function getResult(): Result;

    public function getSourceRepository(): SourceRepository;

    public function getSourceToTargetMap(): SourceToTargetMap;

    public function import(?Filter $filter = null, int $perCycle = 200): Result;

    /**
     * @return \Illuminate\Support\Collection<int, \Smorken\LazyImport\Contracts\Data\TargetData>
     */
    public function sample(?Filter $filter = null, int $sampleSize = 5): Collection;
}
