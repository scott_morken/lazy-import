<?php

declare(strict_types=1);

namespace Smorken\LazyImport\Contracts\ModelCreators;

use Illuminate\Contracts\Support\MessageBag;
use Smorken\LazyImport\Contracts\Data\TargetData;

interface Creator
{
    public function add(TargetData $targetData): void;

    public function getMessages(): MessageBag;

    public function tryCreate(bool $force = false): false|int;
}
