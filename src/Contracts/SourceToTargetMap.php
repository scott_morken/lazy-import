<?php

declare(strict_types=1);

namespace Smorken\LazyImport\Contracts;

use Smorken\LazyImport\Contracts\Data\TargetData;

interface SourceToTargetMap
{
    public function __construct(
        string $targetDataClass,
        mixed $sourceModel = null,
    );

    public function addInvalidSourceMessage(): bool;

    public function identifier(bool $toString = true): string|int|array;

    /**
     * @return array<string, string|\Closure>
     */
    public function map(): array;

    public function newInstance(mixed $model): static;

    public function sourceModel(): mixed;

    public function toTargetData(): TargetData;

    public function validSource(): bool;
}
