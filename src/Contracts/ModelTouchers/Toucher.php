<?php

declare(strict_types=1);

namespace Smorken\LazyImport\Contracts\ModelTouchers;

use Illuminate\Contracts\Support\MessageBag;
use Illuminate\Support\Collection;

interface Toucher
{
    public function getMessages(): MessageBag;

    public function touch(Collection $models): int;
}
