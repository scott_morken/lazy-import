<?php

declare(strict_types=1);

namespace Smorken\LazyImport\Contracts\Converters;

interface IteratorConverter
{
    public function convert(mixed $itemFromIterator): \Iterator;
}
