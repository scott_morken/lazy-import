<?php

declare(strict_types=1);

namespace Smorken\LazyImport\Mailables;

use Illuminate\Mail\Mailable;
use Illuminate\Mail\Mailables\Content;
use Illuminate\Mail\Mailables\Envelope;
use Smorken\LazyImport\Contracts\Result;

class ImportResultMailable extends Mailable
{
    public function __construct(protected Result $result, protected ?string $fromEmail) {}

    public function content(): Content
    {
        return new Content(
            markdown: 'sm-lazyimport::import-result-notification',
            with: [
                'result' => $this->result,
            ]
        );
    }

    public function envelope(): Envelope
    {
        return new Envelope(
            from: $this->fromEmail,
            subject: $this->getSubject()
        );
    }

    protected function getSubject(): string
    {
        return 'Import Result for '.$this->result->getName();
    }
}
