<?php

declare(strict_types=1);

namespace Smorken\LazyImport\ModelTouchers;

use Illuminate\Contracts\Support\MessageBag;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Smorken\LazyImport\Constants\ErrorKey;
use Smorken\LazyImport\Contracts\ModelTouchers\Toucher;

class SingleEloquentToucher implements Toucher
{
    protected \Illuminate\Support\MessageBag $messages;

    public function __construct(protected Model $model)
    {
        $this->messages = new \Illuminate\Support\MessageBag;
    }

    public function getMessages(): MessageBag
    {
        return $this->messages;
    }

    public function touch(Collection $models): int
    {
        $touched = 0;
        foreach ($models as $m) {
            if (! $m->touch()) {
                $this->getMessages()->add(ErrorKey::TOUCH_ERROR->value, 'Model: '.(string) $m);

                continue;
            }
            $touched++;
        }

        return $touched;
    }
}
