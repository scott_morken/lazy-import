<?php

declare(strict_types=1);

namespace Smorken\LazyImport\ModelTouchers;

use Carbon\Carbon;
use Illuminate\Contracts\Support\MessageBag;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Smorken\LazyImport\Contracts\ModelTouchers\Toucher;

class BatchEloquentToucher implements Toucher
{
    protected const UPDATED_AT = 'updated_at';

    protected \Illuminate\Support\MessageBag $messages;

    public function __construct(protected Model $model)
    {
        $this->messages = new \Illuminate\Support\MessageBag;
    }

    public function getMessages(): MessageBag
    {
        return $this->messages;
    }

    public function touch(Collection $models): int
    {
        if ($models->isEmpty()) {
            return 0;
        }
        $keys = $models->map(fn (Model $m) => $m->getKey());
        $keyName = $this->model->getKeyName();

        return $this->model->newQuery()
            ->whereIn($keyName, $keys->toArray())
            ->update([self::UPDATED_AT => Carbon::now()]);
    }
}
