<?php

declare(strict_types=1);

namespace Smorken\LazyImport;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\LazyCollection;
use Smorken\LazyImport\Constants\CounterKey;
use Smorken\LazyImport\Constants\ErrorKey;
use Smorken\LazyImport\Contracts\Actions\CleanupAction;
use Smorken\LazyImport\Contracts\Actions\ImportAction;
use Smorken\LazyImport\Contracts\Actions\Results\ImportResult;
use Smorken\LazyImport\Contracts\LookupCache;
use Smorken\LazyImport\Contracts\Repositories\SourceRepository;
use Smorken\LazyImport\Contracts\Result;
use Smorken\LazyImport\Contracts\SourceToTargetMap;
use Smorken\LazyImport\Events\ResultAvailable;
use Smorken\LazyImport\Lookups\ArrayLookupCache;
use Smorken\LazyImport\Support\IsA;
use Smorken\Support\Contracts\Filter;

class Importer implements \Smorken\LazyImport\Contracts\Importer
{
    protected LookupCache $lookupCache;

    protected Result $result;

    public function __construct(
        protected SourceRepository $sourceRepository,
        protected SourceToTargetMap $sourceToTargetMap,
        protected ImportAction $importAction,
        protected ?CleanupAction $cleanupAction = null,
        ?LookupCache $lookupCache = null,
        ?Result $result = null,
    ) {
        if (! $result) {
            $result = new \Smorken\LazyImport\Result($this->getBaseNameForResult());
        }
        $this->result = $result;
        if ($lookupCache === null) {
            $lookupCache = new ArrayLookupCache;
        }
        $this->lookupCache = $lookupCache;
        $this->init();
    }

    public function cleanup(?Filter $filter = null): Result
    {
        if ($this->getCleanupAction()) {
            $r = ($this->getCleanupAction())($filter);
            $this->increment(CounterKey::CLEANUP, $r->count);
            // @phpstan-ignore argument.type
            $this->getResult()->getMessages()->merge($this->getCleanupAction()->getMessages());
        }

        return $this->getResult();
    }

    public function dispatchResults(): void
    {
        Event::dispatch(new ResultAvailable($this->getResult()));
    }

    public function getCleanupAction(): ?CleanupAction
    {
        return $this->cleanupAction;
    }

    public function getImportAction(): ImportAction
    {
        return $this->importAction;
    }

    public function getLookupCache(): LookupCache
    {
        return $this->lookupCache;
    }

    public function getResult(): Result
    {
        return $this->result->ensure();
    }

    public function getSourceRepository(): SourceRepository
    {
        return $this->sourceRepository;
    }

    public function getSourceToTargetMap(): SourceToTargetMap
    {
        return $this->sourceToTargetMap;
    }

    public function import(?Filter $filter = null, int $perCycle = 200): Result
    {
        $this->getResult()->start();
        $filter = $this->modifyImportFilter($filter);
        $this->preImportCycle();
        $lazyCollection = $this->getLazyCollection($filter);
        /** @var \Iterator $iterator */
        $iterator = $lazyCollection->getIterator();
        while ($iterator->valid()) {
            $mapped = $this->executeCycle($iterator, $perCycle);
            if ($mapped->isEmpty()) {
                break;
            }
            $this->preImport($mapped);
            $importResult = ($this->getImportAction())($mapped);
            $this->postImport($importResult, $mapped);
            $this->addImportResultToResults($importResult);
        }
        $this->postImportCycle();
        // @phpstan-ignore argument.type
        $this->getResult()->getMessages()->merge($this->getImportAction()->getMessages());
        $this->cleanup($filter);
        $this->dispatchResults();

        return $this->getResult();
    }

    public function sample(?Filter $filter = null, int $sampleSize = 5): Collection
    {
        $this->getResult()->start();
        $filter = $this->modifyImportFilter($filter);
        /** @var \Iterator $iterator */
        $iterator = $this->getLazyCollection($filter)->getIterator();

        return $this->executeCycle($iterator, $sampleSize)
            ->map(fn (SourceToTargetMap $map) => $map->toTargetData());
    }

    protected function addImportResultToResults(ImportResult $importResult): void
    {
        $this->increment(CounterKey::EXISTING, $importResult->existing);
        $this->increment(CounterKey::CREATED, $importResult->created);
        $this->increment(CounterKey::UPDATED, $importResult->updated);
        $this->increment(CounterKey::TOUCHED, $importResult->touched);
    }

    protected function continueLazyIteration(SourceToTargetMap $map, mixed $item, int $currentCount): bool
    {
        if ($this->getLookupCache()->has($map->identifier())) {
            return true;
        }
        if (! $map->validSource()) {
            $this->increment(CounterKey::TOTAL);
            if ($map->addInvalidSourceMessage()) {
                $this->getResult()
                    ->addMessage(
                        ErrorKey::VALIDATION,
                        $this->messageWithItem('Invalid source ['.$currentCount.'] '.$map->identifier(), $item)
                    );
            }

            return true;
        }

        return false;
    }

    protected function executeCycle(\Iterator $iterator, int $perCycle = 200): Collection
    {
        $count = 0;
        $mapped = new Collection;

        while ($count < $perCycle) {
            $item = $iterator->current();
            if (! $iterator->valid()) {
                break;
            }
            if ($item) {
                $m = $this->getSourceToTargetMap()->newInstance($item);
                if ($this->continueLazyIteration($m, $item, $count)) {
                    $count++;
                    $iterator->next();

                    continue;
                }
                $this->getLookupCache()->add($m->identifier());
                $mapped->push($m);
                $this->increment(CounterKey::VALIDATED);
                $this->increment(CounterKey::TOTAL);
            }
            $count++;
            $iterator->next();
        }

        return $mapped;
    }

    protected function getBaseNameForResult(): string
    {
        return class_basename($this);
    }

    protected function getLazyCollection(?Filter $filter): LazyCollection
    {
        return ($this->getSourceRepository())($filter);
    }

    protected function increment(CounterKey|string $key, int $count = 1): void
    {
        $this->getResult()->increment($key, $count);
    }

    protected function init(): void
    {
        // override
    }

    protected function itemToString(mixed $item): ?string
    {
        if (is_array($item)) {
            return implode(', ', $item);
        }
        if (IsA::stringable($item)) {
            return (string) $item;
        }

        return null;
    }

    protected function messageWithItem(string $message, mixed $item): string
    {
        $str = $this->itemToString($item);
        if ($str === null) {
            return $message;
        }

        return $message.' ('.$this->itemToString($item).')';
    }

    protected function modifyImportFilter(?Filter $filter): ?Filter
    {
        return $filter;
    }

    protected function postImport(ImportResult $result, Collection $mapped): void
    {
        //
    }

    protected function postImportCycle(): void
    {
        //
    }

    protected function preImport(Collection $mapped): void
    {
        //
    }

    protected function preImportCycle(): void
    {
        //
    }
}
