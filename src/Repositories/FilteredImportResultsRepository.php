<?php

declare(strict_types=1);

namespace Smorken\LazyImport\Repositories;

use Smorken\Domain\Repositories\EloquentFilteredRepository;
use Smorken\LazyImport\Contracts\Models\ImportResult;

/**
 * @template TModel of \Smorken\LazyImport\Models\Eloquent\ImportResult
 *
 * @extends EloquentFilteredRepository<TModel>
 */
class FilteredImportResultsRepository extends EloquentFilteredRepository implements \Smorken\LazyImport\Contracts\Repositories\FilteredImportResultsRepository
{
    public function __construct(ImportResult $model)
    {
        parent::__construct($model);
    }
}
