<?php

declare(strict_types=1);

namespace Smorken\LazyImport\Repositories;

use Illuminate\Database\Eloquent\Model as EloquentModel;
use Illuminate\Support\Collection;
use Smorken\Domain\Repositories\Concerns\RepositoryFromEloquent;
use Smorken\Domain\Repositories\Repository;
use Smorken\LazyImport\Contracts\Data\TargetData;
use Smorken\LazyImport\Contracts\Repositories\TargetRepository;
use Smorken\Model\Contracts\Model;

/**
 * @template TModel of Model|EloquentModel
 *
 * @extends Repository<TModel>
 */
class EloquentTargetRepository extends Repository implements TargetRepository
{
    /** @use RepositoryFromEloquent<TModel> */
    use RepositoryFromEloquent;

    public function __construct(protected Model|EloquentModel $model) {}

    public function __invoke(Collection $targetDatum): Collection
    {
        $identifiers = $targetDatum->map(fn (TargetData $datum) => $datum->identifier())->toArray();

        return $this->getModelsFromIdentifiers($identifiers);
    }

    protected function getModelsFromIdentifiers(array $identifiers): Collection
    {
        // @phpstan-ignore method.notFound
        return $this->query()->identifiers($identifiers)->get($this->getColumns());
    }
}
