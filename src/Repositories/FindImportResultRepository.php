<?php

declare(strict_types=1);

namespace Smorken\LazyImport\Repositories;

use Smorken\Domain\Repositories\EloquentRetrieveRepository;
use Smorken\LazyImport\Contracts\Models\ImportResult;

/**
 * @template TModel of \Smorken\LazyImport\Models\Eloquent\ImportResult
 *
 * @extends EloquentRetrieveRepository<TModel>
 */
class FindImportResultRepository extends EloquentRetrieveRepository implements \Smorken\LazyImport\Contracts\Repositories\FindImportResultRepository
{
    public function __construct(ImportResult $model)
    {
        parent::__construct($model);
    }
}
