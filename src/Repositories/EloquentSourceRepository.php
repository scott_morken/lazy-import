<?php

declare(strict_types=1);

namespace Smorken\LazyImport\Repositories;

use Smorken\Domain\Repositories\EloquentLazyCollectionRepository;
use Smorken\LazyImport\Contracts\Repositories\SourceRepository;

/**
 * @template TModel of \Smorken\Model\Contracts\Model|\Illuminate\Database\Eloquent\Model
 *
 * @extends EloquentLazyCollectionRepository<TModel>
 */
class EloquentSourceRepository extends EloquentLazyCollectionRepository implements SourceRepository {}
