<?php

declare(strict_types=1);

namespace Smorken\LazyImport\Events;

use Smorken\LazyImport\Contracts\Result;

class ResultAvailable
{
    public function __construct(public Result $result) {}
}
