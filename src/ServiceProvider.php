<?php

declare(strict_types=1);

namespace Smorken\LazyImport;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\Mail\Mailer;
use Smorken\LazyImport\Console\Commands\CleanupImportResultModels;
use Smorken\LazyImport\Listeners\SendImportResultNotification;

class ServiceProvider extends \Illuminate\Support\ServiceProvider
{
    public function boot(): void
    {
        $this->bootConfig();
        $this->bootViews();
        $this->bootMigrations();
        $this->bootCommands();
    }

    public function register(): void
    {
        $this->registerModels();
        $this->registerRepositories();
        $this->registerActions();
        $this->registerSendImportResultNotification();
    }

    protected function bootCommands(): void
    {
        $this->commands([
            CleanupImportResultModels::class,
        ]);
    }

    protected function bootConfig(): void
    {
        $config = __DIR__.'/../config/config.php';
        $this->mergeConfigFrom($config, 'sm-lazyimport');
        $this->publishes([
            __DIR__.'/../config/config.php' => config_path('sm-lazyimport.php'), // @phpstan-ignore function.notFound
        ], 'config');
    }

    protected function bootMigrations(): void
    {
        if ($this->app['config']->get('sm-lazyimport.migrations', false)) {
            $this->loadMigrationsFrom(__DIR__.'/../database/migrations');
        }
    }

    protected function bootViews(): void
    {
        $this->loadViewsFrom(__DIR__.'/../views', 'sm-lazyimport');
        $this->publishes([
            __DIR__.'/../views' => resource_path('/views/vendor/smorken/lazyimport'), // @phpstan-ignore function.notFound
        ], 'views');
    }

    protected function registerActions(): void
    {
        $this->booted(function (Application $app) {
            foreach ($app['config']->get('sm-lazyimport.actions', []) as $contract => $impl) {
                $app->scoped($contract, static fn (Application $app) => $app[$impl]);
            }
        });
    }

    protected function registerModels(): void
    {
        $this->booted(function (Application $app) {
            foreach ($app['config']->get('sm-lazyimport.models', []) as $contract => $impl) {
                $app->scoped($contract, static fn (Application $app) => $app[$impl]);
            }
        });
    }

    protected function registerRepositories(): void
    {
        $this->booted(function (Application $app) {
            foreach ($app['config']->get('sm-lazyimport.repositories', []) as $contract => $impl) {
                $app->scoped($contract, static fn (Application $app) => $app[$impl]);
            }
        });
    }

    protected function registerSendImportResultNotification(): void
    {
        $this->app->scoped(SendImportResultNotification::class, function (Application $app) {
            $from = $app['config']->get('sm-lazyimport.notifications.from');
            $to = $app['config']->get('sm-lazyimport.notifications.to');

            return new SendImportResultNotification($app[Mailer::class], $to, $from);
        });
    }
}
