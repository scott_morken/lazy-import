<?php

declare(strict_types=1);

namespace Smorken\LazyImport;

use Carbon\Carbon;
use Illuminate\Contracts\Support\MessageBag;
use Smorken\LazyImport\Constants\CounterKey;
use Smorken\LazyImport\Constants\ErrorKey;

class Result implements \Smorken\LazyImport\Contracts\Result
{
    protected array $counters = [];

    protected Carbon $end;

    protected float $mem = 0.0;

    protected MessageBag $messages;

    protected Carbon $start;

    protected bool $started = false;

    protected bool $stopped = false;

    public function __construct(protected string $base)
    {
        $this->messages = new \Illuminate\Support\MessageBag;
    }

    public function addMessage(ErrorKey|string $key, string $message): void
    {
        $key = $this->ensureString($key);
        $this->getMessages()->add($this->getKey($key), $message);
    }

    public function decrement(CounterKey|string $key, int $count = 1): void
    {
        $key = $this->ensureString($key);
        $this->changeCount($key, $count * -1);
    }

    public function ensure(): self
    {
        $this->start();

        return $this;
    }

    public function getCounter(CounterKey|string $key): int
    {
        $key = $this->ensureString($key);

        return $this->counters[$this->getKey($key)] ?? 0;
    }

    public function getCounters(): array
    {
        return $this->counters;
    }

    public function setCounters(array $counters): void
    {
        foreach ($counters as $k => $v) {
            $this->setCounter($k, $v);
        }
    }

    public function getEnd(): ?Carbon
    {
        $this->stop();

        return $this->end;
    }

    public function getMemoryUse(): float
    {
        if (! $this->mem) {
            $this->mem = memory_get_peak_usage(true) / (2 ** 20);
        }

        return $this->mem;
    }

    public function getMessages(): MessageBag
    {
        return $this->messages;
    }

    public function getName(): string
    {
        return $this->base;
    }

    public function getStart(): ?Carbon
    {
        $this->start();

        return $this->start;
    }

    public function increment(CounterKey|string $key, int $count = 1): void
    {
        $key = $this->ensureString($key);
        $this->changeCount($key, $count);
    }

    public function initCounters(array $counters = []): void
    {
        $this->counters = [];
        foreach ($counters as $counter) {
            $this->reset($counter);
        }
    }

    public function reset(CounterKey|string $key): void
    {
        $key = $this->ensureString($key);
        $this->setCounter($this->getKey($key), 0);
    }

    public function resetAll(): void
    {
        $this->started = false;
        $this->stopped = false;
        $this->messages = new \Illuminate\Support\MessageBag;
        $this->initCounters(array_keys($this->getCounters()));
    }

    public function setCounter(CounterKey|string $key, int $value): void
    {
        $key = $this->ensureString($key);
        $this->counters[$this->getKey($key)] = $value;
    }

    public function start(): void
    {
        if (! $this->started) {
            $this->start = Carbon::now();
            $this->started = true;
        }
    }

    public function stop(): void
    {
        if (! $this->stopped) {
            $this->getMemoryUse();
            $this->end = Carbon::now();
            $this->stopped = true;
        }
    }

    public function toArray(): array
    {
        $this->start();
        $this->stop();

        return array_merge($this->getCounters(), [
            'start' => $this->getStart(),
            'end' => $this->getEnd(),
            'memory' => $this->getMemoryUse(),
            'messages' => $this->getMessages(),
        ]);
    }

    protected function changeCount(string $key, int $count): void
    {
        $current = $this->getCounter($key);
        $this->counters[$this->getKey($key)] = $current + $count;
    }

    protected function ensureString(\BackedEnum|string $value): string
    {
        if (is_a($value, \BackedEnum::class)) {
            return $value->value;
        }

        return $value;
    }

    protected function getKey(string $key): string
    {
        if (str_contains($key, '::')) {
            return $key;
        }

        return $this->base.'::'.$key;
    }
}
