<?php

declare(strict_types=1);

namespace Smorken\LazyImport\Models\Eloquent;

use Illuminate\Database\Eloquent\HasBuilder;
use Smorken\LazyImport\Models\Builder\ImportResultBuilder;
use Smorken\Model\Eloquent;

class ImportResult extends Eloquent implements \Smorken\LazyImport\Contracts\Models\ImportResult
{
    /** @use HasBuilder<ImportResultBuilder<static>> */
    use HasBuilder;

    protected static string $builder = ImportResultBuilder::class;

    protected $casts = [
        'data' => 'array',
    ];

    protected $fillable = ['importer', 'data'];
}
