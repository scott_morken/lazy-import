<?php

declare(strict_types=1);

namespace Smorken\LazyImport\Models\Concerns;

use Illuminate\Contracts\Database\Query\Builder;

trait HasIdentifiersInternal
{
    protected function buildIdentifierFromMultipleKeys(Builder $query, array $identifier): Builder
    {
        return $query->orWhere(function (Builder $q) use ($identifier) {
            foreach ($identifier as $key => $value) {
                $this->buildIdentifierIs($q, $key, $value, 'and');
            }
        });
    }

    protected function buildIdentifierIn(Builder $query, array $identifiers): Builder
    {
        return $query->where(function (Builder $q) use ($identifiers) {
            foreach ($identifiers as $identifier) {
                $this->buildIdentifierFromMultipleKeys($q, $identifier);
            }
        });
    }

    protected function buildIdentifierIs(Builder $query, string $key, $value, string $boolean = 'or'): Builder
    {
        return $query->where($key, '=', $value, $boolean);
    }

    protected function buildIdentifiers(Builder $query, array $identifiers): Builder
    {
        return $query->where(function (Builder $q) use ($identifiers) {
            foreach ($identifiers as $identifier) {
                if ($this->isSingleIdentifier($identifier)) {
                    $this->buildIdentifierIs($q, key($identifier), current($identifier));
                } else {
                    $this->buildIdentifierFromMultipleKeys($q, $identifier);
                }
            }
        });
    }

    protected function isSingleIdentifier(array $identifier): bool
    {
        return count($identifier) === 1;
    }
}
