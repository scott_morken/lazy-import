<?php

declare(strict_types=1);

namespace Smorken\LazyImport\Models\Concerns;

use Illuminate\Contracts\Database\Query\Builder;

trait HasIdentifiersOnModel
{
    use HasIdentifiersInternal;

    public function scopeIdentifierIn(Builder $query, array $identifiers): Builder
    {
        return $this->buildIdentifierIn($query, $identifiers, 'and');
    }

    public function scopeIdentifierIs(Builder $query, string $key, mixed $value): Builder
    {
        return $this->buildIdentifierIs($query, $key, $value, 'and');
    }

    public function scopeIdentifiers(Builder $query, array $identifiers): Builder
    {
        return $this->buildIdentifiers($query, $identifiers);
    }
}
