<?php

declare(strict_types=1);

namespace Smorken\LazyImport\Models\Concerns;

use Illuminate\Contracts\Database\Query\Builder;

trait HasIdentifiersOnBuilder
{
    use HasIdentifiersInternal;

    public function identifierIn(array $identifiers): Builder
    {
        return $this->buildIdentifierIn($this, $identifiers, 'and');
    }

    public function identifierIs(string $key, mixed $value): Builder
    {
        return $this->buildIdentifierIs($this, $key, $value, 'and');
    }

    public function identifiers(array $identifiers): Builder
    {
        return $this->buildIdentifiers($this, $identifiers);
    }
}
