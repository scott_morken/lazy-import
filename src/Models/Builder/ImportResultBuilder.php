<?php

declare(strict_types=1);

namespace Smorken\LazyImport\Models\Builder;

use Carbon\Carbon;
use Smorken\Model\Filters\FilterHandler;
use Smorken\Model\QueryBuilders\Builder;
use Smorken\Model\QueryBuilders\Concerns\HasIdIsScope;
use Smorken\QueryStringFilter\Concerns\WithQueryStringFilter;

/**
 * @template TModel of \Smorken\LazyImport\Models\Eloquent\ImportResult
 *
 * @extends Builder<TModel>
 */
class ImportResultBuilder extends Builder
{
    use HasIdIsScope, WithQueryStringFilter;

    /**
     * @return static<TModel>
     */
    public function createdAfter(Carbon|string $date): self
    {
        return $this->where('created_at', '>=', $this->ensureCarbon($date));
    }

    /**
     * @return static<TModel>
     */
    public function createdBefore(Carbon|string $date): self
    {
        return $this->where('created_at', '<=', $this->ensureCarbon($date));
    }

    public function defaultOrder(): self
    {
        /** @var $this<TModel> */
        return $this->latest();
    }

    /**
     * @return static<TModel>
     */
    public function importerIs(string $importer): self
    {
        return $this->where('importer', '=', $importer);
    }

    protected function ensureCarbon(Carbon|string $date): Carbon
    {
        if (is_a($date, Carbon::class)) {
            return $date;
        }

        return Carbon::parse($date);
    }

    protected function getFilterHandlersForFilters(): array
    {
        return [
            new FilterHandler('importer', 'importerIs'),
            new FilterHandler('createdAfter', 'createdAfter'),
            new FilterHandler('createdBefore', 'createdBefore'),
        ];
    }
}
