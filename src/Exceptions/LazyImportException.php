<?php

declare(strict_types=1);

namespace Smorken\LazyImport\Exceptions;

class LazyImportException extends \Exception
{
    public static function noSourceIdentifier(): self
    {
        return new self('No source identifier provided');
    }
}
