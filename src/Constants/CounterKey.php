<?php

declare(strict_types=1);

namespace Smorken\LazyImport\Constants;

enum CounterKey: string
{
    case TOTAL = 'total';
    case VALIDATED = 'validated';
    case EXISTING = 'existing';

    case CREATED = 'created';
    case UPDATED = 'updated';
    case CLEANUP = 'cleanup';

    case TOUCHED = 'touched';
}
