<?php

declare(strict_types=1);

namespace Smorken\LazyImport\Constants;

enum ErrorKey: string
{
    case VALIDATION = 'Validation';

    case MISSING_TARGET = 'MissingTarget';

    case CREATE_ERROR = 'CreateError';
    case UPDATE_ERROR = 'UpdateError';
    case TOUCH_ERROR = 'TouchError';

    case ITERATION_ERROR = 'IterationError';
}
