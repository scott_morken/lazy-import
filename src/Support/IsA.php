<?php

declare(strict_types=1);

namespace Smorken\LazyImport\Support;

class IsA
{
    public static function stringable(mixed $value): bool
    {
        return is_string($value) ||
            (is_object($value) && method_exists($value, '__toString')) ||
            (is_a($value, \Stringable::class));
    }
}
