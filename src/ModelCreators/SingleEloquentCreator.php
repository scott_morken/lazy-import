<?php

declare(strict_types=1);

namespace Smorken\LazyImport\ModelCreators;

use Illuminate\Contracts\Support\MessageBag;
use Illuminate\Database\Eloquent\Model;
use Smorken\LazyImport\Constants\ErrorKey;
use Smorken\LazyImport\Contracts\Data\TargetData;
use Smorken\LazyImport\Contracts\ModelCreators\Creator;

class SingleEloquentCreator implements Creator
{
    protected const CREATE_FAILED = 0;

    protected const CREATE_SUCCESS = 1;

    protected MessageBag $messages;

    protected ?TargetData $toCreate = null;

    public function __construct(protected Model $model)
    {
        $this->messages = new \Illuminate\Support\MessageBag;
        $this->toCreate = null;
    }

    public function add(TargetData $targetData): void
    {
        $this->toCreate = $targetData;
    }

    public function getMessages(): MessageBag
    {
        return $this->messages;
    }

    public function tryCreate(bool $force = false): false|int
    {
        if (! $this->toCreate) {
            return false;
        }
        $m = $this->createInstance($this->toCreate->createAttributes());
        $this->toCreate = null;
        if (! $m->save()) {
            $this->getMessages()->add(ErrorKey::CREATE_ERROR->value, 'Model: '.(string) $m);

            return self::CREATE_FAILED;
        }

        return self::CREATE_SUCCESS;
    }

    private function createInstance(array $attributes): Model
    {
        return $this->model->newInstance($attributes);
    }
}
