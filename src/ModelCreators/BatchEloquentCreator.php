<?php

declare(strict_types=1);

namespace Smorken\LazyImport\ModelCreators;

use Carbon\Carbon;
use Illuminate\Contracts\Support\MessageBag;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Smorken\LazyImport\Constants\ErrorKey;
use Smorken\LazyImport\Contracts\Data\TargetData;
use Smorken\LazyImport\Contracts\ModelCreators\Creator;

class BatchEloquentCreator implements Creator
{
    protected const CREATED_AT = 'created_at';

    protected const UPDATED_AT = 'updated_at';

    protected int $batchNumber = 0;

    protected bool $batchSizeSet = false;

    protected int $maxBoundAttributes = 2000;

    protected MessageBag $messages;

    protected Collection $toCreate;

    public function __construct(protected Model $model, protected int $batchSize = 0)
    {
        $this->messages = new \Illuminate\Support\MessageBag;
        $this->toCreate = new Collection;
    }

    public function add(TargetData $targetData): void
    {
        $this->toCreate->push($this->getPreparedAttributes($targetData));
    }

    public function getMessages(): MessageBag
    {
        return $this->messages;
    }

    public function tryCreate(bool $force = false): false|int
    {
        if (! $this->shouldCreate() && ! $force) {
            return false;
        }

        if ($this->toCreate->count() !== ($created = $this->executeBatchCreation())) {
            $this->getMessages()
                ->add($this->getMessageKey(), "Expected {$this->toCreate->count()} records, created $created records.");
        }
        $this->toCreate = new Collection;
        $this->batchNumber++;

        return $created;
    }

    protected function ensureBatchSize(array $attributes): void
    {
        $this->batchSizeSet = true;
        if ($this->batchSize !== 0) {
            return;
        }
        if (count($attributes) === 0) {
            return;
        }
        $this->batchSize = (int) floor($this->maxBoundAttributes / count($attributes));
    }

    protected function executeBatchCreation(): int
    {
        if ($this->toCreate->count() === 0) {
            return 0;
        }

        return $this->model->query()->insertOrIgnore($this->toCreate->toArray());
    }

    protected function getMessageKey(): string
    {
        return ErrorKey::CREATE_ERROR->value.':batch-'.$this->batchNumber;
    }

    protected function getPreparedAttributes(TargetData $targetData): array
    {
        $attributes = $targetData->createAttributes();
        $this->ensureBatchSize($attributes);

        if ($this->model->timestamps) {
            $now = Carbon::now();
            $attributes[self::CREATED_AT] = $now;
            $attributes[self::UPDATED_AT] = $now;
        }

        return $attributes;
    }

    protected function shouldCreate(): bool
    {
        if ($this->batchSize === 0) {
            if ($this->batchSizeSet) {
                $this->getMessages()
                    ->add($this->getMessageKey(),
                        'No batch size has been set using attributes. Cannot create records.');
            }

            return false;
        }

        return $this->toCreate->count() >= $this->batchSize;
    }
}
