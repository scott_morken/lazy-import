<?php

declare(strict_types=1);

namespace Smorken\LazyImport\Http\Controllers\Web;

use Illuminate\Http\Request;
use Smorken\Controller\Contracts\View\WithResource\WithFilteredRepository;
use Smorken\Controller\Contracts\View\WithResource\WithRetrieveRepository;
use Smorken\Controller\View\Controller;
use Smorken\Controller\View\WithResource\Concerns\HasFilteredIndex;
use Smorken\Controller\View\WithResource\Concerns\HasShow;
use Smorken\LazyImport\Contracts\Repositories\FilteredImportResultsRepository;
use Smorken\LazyImport\Contracts\Repositories\FindImportResultRepository;
use Smorken\QueryStringFilter\QueryStringFilter;

class ImportResultController extends Controller implements WithFilteredRepository, WithRetrieveRepository
{
    use HasFilteredIndex, HasShow;

    protected string $baseView = 'sm-lazyimport::import-result';

    public function __construct(
        FilteredImportResultsRepository $filteredRepository,
        FindImportResultRepository $retrieveRepository,
    ) {
        parent::__construct();
        $this->filteredRepository = $filteredRepository;
        $this->retrieveRepository = $retrieveRepository;
    }

    public function getFilterFromRequest(Request $request): \Smorken\QueryStringFilter\Contracts\QueryStringFilter
    {
        return QueryStringFilter::from($request)
            ->setFilters(['importer', 'createdAfter', 'createdBefore'])
            ->addKeyValue('page');
    }
}
