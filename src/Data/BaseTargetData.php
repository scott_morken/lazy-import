<?php

declare(strict_types=1);

namespace Smorken\LazyImport\Data;

use Smorken\LazyImport\Contracts\Data\TargetData;

abstract class BaseTargetData implements TargetData
{
    protected array $updateAttributes = [];

    public function __construct(protected array $identifier) {}

    public function createAttributes(): array
    {
        return $this->toArray();
    }

    public function identifier(): array
    {
        return $this->identifier;
    }

    public function toArray(): array
    {
        return $this->arrayFromPublicProperties();
    }

    public function updateAttributes(): array
    {
        if (empty($this->updateAttributes)) {
            return $this->toArray();
        }
        $attributes = [];
        foreach ($this->toArray() as $k => $v) {
            if (in_array($k, $this->updateAttributes, true)) {
                $attributes[$k] = $v;
            }
        }

        return $attributes;
    }

    protected function arrayFromPublicProperties(): array
    {
        $ref = new \ReflectionClass($this);
        $props = [];
        $reflectionProps = $ref->getProperties(\ReflectionProperty::IS_PUBLIC);
        foreach ($reflectionProps as $prop) {
            $props[$prop->getName()] = $this->{$prop->getName()};
        }

        return $props;
    }
}
