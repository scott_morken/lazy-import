<?php

declare(strict_types=1);

namespace Smorken\LazyImport\Converters;

use Smorken\LazyImport\Contracts\Converters\IteratorConverter;

class ArrayIteratorConverter implements IteratorConverter
{
    public function convert(mixed $itemFromIterator): \Iterator
    {
        return new \ArrayIterator([$itemFromIterator]);
    }
}
