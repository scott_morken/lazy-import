<?php

declare(strict_types=1);

namespace Smorken\LazyImport\Lookups;

use Smorken\LazyImport\Contracts\LookupCache;

class ArrayLookupCache implements LookupCache
{
    protected array $cache = [];

    public function add(string $key): void
    {
        $this->cache[$key] = true;
    }

    public function has(string $key): bool
    {
        return isset($this->cache[$key]);
    }

    public function reset(): void
    {
        $this->cache = [];
    }
}
