<?php

declare(strict_types=1);

namespace Smorken\LazyImport\Actions;

use Carbon\Carbon;
use Illuminate\Contracts\Database\Query\Builder;
use Illuminate\Database\Eloquent\Model as EloquentModel;
use Smorken\Domain\Actions\ActionWithEloquent;
use Smorken\Domain\Models\Concerns\WithBuilderHasQuery;
use Smorken\LazyImport\Contracts\Actions\CleanupAction;
use Smorken\LazyImport\Contracts\Actions\Results\CleanupResult;
use Smorken\Model\Contracts\Model;
use Smorken\Support\Contracts\Filter;

/**
 * @template TModel of EloquentModel|Model
 *
 * @extends ActionWithEloquent<TModel>
 */
abstract class EloquentCleanupAction extends ActionWithEloquent implements CleanupAction
{
    use WithBuilderHasQuery;

    protected string $cleanupFilterKey = 'cleanupBefore';

    public function __construct(EloquentModel|Model $model, protected $cleanupBefore = '-14 days')
    {
        parent::__construct($model);
    }

    public function __invoke(?Filter $filter): CleanupResult
    {
        $filter = $this->modifyFilter($filter);
        if (! $this->canCleanup($filter)) {
            return new \Smorken\LazyImport\Actions\Results\CleanupResult(0);
        }
        $this->preCleanup();
        $count = $this->cleanup($filter);
        $this->postCleanup();

        return new \Smorken\LazyImport\Actions\Results\CleanupResult($count);
    }

    protected function canCleanup(?Filter $filter): bool
    {
        return $filter && $filter->has($this->cleanupFilterKey);
    }

    protected function cleanup(Filter $filter): int
    {
        $query = $this->createCleanupQuery($this->eloquentModel()->newQuery(), $filter);
        $query = $this->modifyQuery($query);

        return $this->executeCleanupQuery($query);
    }

    protected function createCleanupQuery(Builder $query, Filter $filter): Builder
    {
        $date = $this->getDateFromFilter($filter);
        /** @var \Illuminate\Contracts\Database\Eloquent\Builder $query */
        if ($this->builderHasQuery('filter', $query)) {
            // @phpstan-ignore method.notFound
            $query->filter(\Smorken\Support\Filter::fromArray($filter->except(['cleanupBefore'])));
        }
        if ($this->builderHasQuery('cleanupBefore', $query)) {
            // @phpstan-ignore method.notFound
            return $query->cleanupBefore($date);
        }

        return $query->whereDate('updated_at', '<', $date);
    }

    protected function ensureCleanupFilter(?Filter $filter): Filter
    {
        if (! $filter) {
            $filter = new \Smorken\Support\Filter;
        }
        if (! $filter->has($this->cleanupFilterKey)) {
            $date = Carbon::parse($this->cleanupBefore)->format('Y-m-d');
            $filter->setAttribute($this->cleanupFilterKey, $date);
        }

        return $filter;
    }

    protected function executeCleanupQuery(Builder $query): int
    {
        return $query->delete();
    }

    protected function getDateFromFilter(Filter $filter): string|Carbon
    {
        return $filter->getAttribute($this->cleanupFilterKey);
    }

    protected function modifyFilter(?Filter $filter): ?Filter
    {
        return $this->ensureCleanupFilter($filter);
    }

    protected function modifyQuery(Builder $query): Builder
    {
        return $query;
    }

    protected function postCleanup(): void
    {
        //
    }

    protected function preCleanup(): void
    {
        //
    }
}
