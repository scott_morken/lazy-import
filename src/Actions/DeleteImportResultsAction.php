<?php

declare(strict_types=1);

namespace Smorken\LazyImport\Actions;

use Carbon\Carbon;
use Smorken\Domain\Actions\ActionWithEloquent;
use Smorken\LazyImport\Contracts\Models\ImportResult;

/**
 * @template TModel of \Smorken\LazyImport\Models\Eloquent\ImportResult
 *
 * @extends ActionWithEloquent<TModel>
 */
class DeleteImportResultsAction extends ActionWithEloquent implements \Smorken\LazyImport\Contracts\Actions\DeleteImportResultsAction
{
    public function __construct(ImportResult $model)
    {
        parent::__construct($model);
    }

    public function __invoke(Carbon|string $olderThan = '-60 days'): int
    {
        if (is_string($olderThan)) {
            $olderThan = Carbon::parse($olderThan);
        }

        return $this->eloquentModel()->newQuery()->where('created_at', '<', $olderThan)->delete();
    }
}
