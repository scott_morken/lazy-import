<?php

declare(strict_types=1);

namespace Smorken\LazyImport\Actions\Results;

class ImportResult implements \Smorken\LazyImport\Contracts\Actions\Results\ImportResult
{
    public function __construct(
        public int $existing,
        public int $created,
        public int $updated,
        public int $touched,
    ) {}
}
