<?php

declare(strict_types=1);

namespace Smorken\LazyImport\Actions\Results;

class CleanupResult implements \Smorken\LazyImport\Contracts\Actions\Results\CleanupResult
{
    public function __construct(public int $count) {}
}
