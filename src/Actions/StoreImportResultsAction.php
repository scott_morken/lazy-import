<?php

declare(strict_types=1);

namespace Smorken\LazyImport\Actions;

use Smorken\Domain\Actions\ActionWithEloquent;
use Smorken\LazyImport\Contracts\Models\ImportResult;
use Smorken\LazyImport\Contracts\Result;

/**
 * @template TModel of \Smorken\LazyImport\Models\Eloquent\ImportResult
 *
 * @extends ActionWithEloquent<TModel>
 */
class StoreImportResultsAction extends ActionWithEloquent implements \Smorken\LazyImport\Contracts\Actions\StoreImportResultsAction
{
    public function __construct(ImportResult $model)
    {
        parent::__construct($model);
    }

    public function __invoke(Result $result): ImportResult
    {
        /** @var ImportResult&\Smorken\Model\Eloquent $m */
        $m = $this->getModelInstance()->fill(['importer' => $result->getName(), 'data' => $result->toArray()]);
        $m->save();

        return $m;
    }
}
