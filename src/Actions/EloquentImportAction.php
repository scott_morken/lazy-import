<?php

declare(strict_types=1);

namespace Smorken\LazyImport\Actions;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Model as EloquentModel;
use Illuminate\Support\Collection;
use Smorken\Domain\Actions\ActionWithEloquent;
use Smorken\LazyImport\Constants\ErrorKey;
use Smorken\LazyImport\Contracts\Actions\ImportAction;
use Smorken\LazyImport\Contracts\Actions\Results\ImportResult;
use Smorken\LazyImport\Contracts\Data\TargetData;
use Smorken\LazyImport\Contracts\ModelCreators\Creator;
use Smorken\LazyImport\Contracts\ModelTouchers\Toucher;
use Smorken\LazyImport\Contracts\Repositories\TargetRepository;
use Smorken\LazyImport\Contracts\SourceToTargetMap;
use Smorken\LazyImport\ModelCreators\SingleEloquentCreator;
use Smorken\LazyImport\ModelTouchers\BatchEloquentToucher;

/**
 * @template TModel of EloquentModel|\Smorken\Model\Contracts\Model
 *
 * @extends ActionWithEloquent<TModel>
 */
abstract class EloquentImportAction extends ActionWithEloquent implements ImportAction
{
    public function __construct(
        EloquentModel|\Smorken\Model\Contracts\Model $model,
        protected TargetRepository $targetRepository,
        protected ?Creator $creator = null,
        protected ?Toucher $toucher = null
    ) {
        parent::__construct($model);
        if (! $this->creator) {
            $this->creator = new SingleEloquentCreator($this->model);
        }

        if (! $this->toucher) {
            $this->toucher = new BatchEloquentToucher($this->model);
        }
    }

    public function __invoke(Collection $mapped): ImportResult
    {
        $targetDatum = $this->getTargetDataCollection($mapped);
        $existing = $this->getExistingModels($targetDatum);
        $created = $this->handleCreate($targetDatum, $existing);
        $updateOrTouchCollection = $this->collectModelsToUpdateOrTouch($targetDatum, $existing);
        $updated = $this->handleUpdate($updateOrTouchCollection);
        $touched = $this->handleTouch($updateOrTouchCollection);
        // @phpstan-ignore argument.type
        $this->getMessages()->merge($this->creator->getMessages());
        // @phpstan-ignore argument.type
        $this->getMessages()->merge($this->toucher->getMessages());

        return new \Smorken\LazyImport\Actions\Results\ImportResult(
            count($existing),
            $created,
            $updated,
            $touched
        );
    }

    public function getTargetRepository(): TargetRepository
    {
        return $this->targetRepository;
    }

    protected function collectModelsToUpdateOrTouch(Collection $targetDatum, Collection $existing): Collection
    {
        $toUpdateOrTouch = new Collection;
        /** @var \Smorken\LazyImport\Contracts\Data\TargetData $targetData */
        foreach ($targetDatum as $targetData) {
            $m = $this->existingModelForTargetData($existing, $targetData);
            if ($m) {
                $m->fill($targetData->updateAttributes());
                $toUpdateOrTouch->push($m);
            }
        }

        return $toUpdateOrTouch;
    }

    protected function collectTargetsToCreate(Collection $targetDatum, Collection $existing): Collection
    {
        $toCreate = new Collection;
        /** @var \Smorken\LazyImport\Contracts\Data\TargetData $targetData */
        foreach ($targetDatum as $targetData) {
            if ($this->existingModelForTargetData($existing, $targetData) === null) {
                $toCreate->push($targetData);
            }
        }

        return $toCreate;
    }

    protected function createFromTargets(Collection $targetDatum): int
    {
        $created = 0;
        foreach ($targetDatum as $data) {
            $this->creator->add($data);
            $created += $this->creator->tryCreate() ?: 0;
        }
        $created += $this->creator->tryCreate(true) ?: 0;

        return $created;
    }

    protected function existingModelForTargetData(Collection $existing, TargetData $targetData): ?Model
    {
        foreach ($existing as $model) {
            if ($this->targetDataMatchesModel($targetData, $model)) {
                return $model;
            }
        }

        return null;
    }

    protected function getExistingModels(Collection $targetDatum): Collection
    {
        return ($this->getTargetRepository())($targetDatum);
    }

    protected function getTargetDataCollection(Collection $mapped): Collection
    {
        return $mapped->map(fn (SourceToTargetMap $map) => $map->toTargetData());
    }

    protected function handleCreate(Collection $targetDatum, Collection $existing): int
    {
        $toCreate = $this->collectTargetsToCreate($targetDatum, $existing);

        return $this->createFromTargets($toCreate);
    }

    protected function handleTouch(Collection $updateOrTouchCollection): int
    {
        $toTouch = $updateOrTouchCollection->filter(fn (EloquentModel $m) => $m->isClean());

        return $this->toucher->touch($toTouch);
    }

    protected function handleUpdate(Collection $updateOrTouchCollection): int
    {
        $updated = 0;
        $toUpdate = $updateOrTouchCollection->filter(fn (EloquentModel $m) => $m->isDirty());
        foreach ($toUpdate as $m) {
            if (! $m->save()) {
                $this->getMessages()->add(ErrorKey::UPDATE_ERROR->value, 'Model: '.(string) $m);

                continue;
            }
            $updated++;
        }

        return $updated;
    }

    protected function isEmpty(mixed $value): bool
    {
        return empty($value) && ! is_numeric($value) && ! is_bool($value);
    }

    protected function targetDataMatchesModel(TargetData $targetDatum, EloquentModel $model): bool
    {
        if (! $this->validTargetData($targetDatum)) {
            return false;
        }
        $identifier = $targetDatum->identifier();
        foreach ($identifier as $k => $v) {
            if ($v != $model->$k) {
                return false;
            }
        }

        return true;
    }

    protected function validTargetData(TargetData $targetDatum): bool
    {
        $identifier = $targetDatum->identifier();
        if (! $identifier) {
            return false;
        }
        foreach ($identifier as $k => $v) {
            if (! $this->isEmpty($v)) {
                return true;
            }
        }

        return false;
    }
}
