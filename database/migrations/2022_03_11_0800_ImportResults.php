<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function down(): void
    {
        Schema::dropIfExists('import_results');
    }

    public function up(): void
    {
        Schema::create('import_results', function (Blueprint $t) {
            $t->bigIncrements('id');
            $t->string('importer');
            $t->mediumText('data');
            $t->timestamps();

            $t->index('importer', 'im_importer_ndx');
        });
    }
};
