<?php
/**
 * @var \Smorken\LazyImport\Contracts\Result $result
 */
?>
<x-mail::message>
# Import Results
## {{ $result->getName() }}

Started: {{ $result->getStart() }}
Ended: {{ $result->getEnd() }}
Memory: {{ $result->getMemoryUse() }}

### Counters

<x-mail::table>
| Counter | Count |
| ------- | ----- |
@foreach ($result->getCounters() as $counter => $count)
| {{ $counter }} | {{ $count }} |
@endforeach
</x-mail::table>

### Messages
@foreach ($result->getMessages() as $key => $messages)
**{{ $key }}**
@foreach ($messages as $message)
  * {{ $message }}
@endforeach
@endforeach
</x-mail::message>
