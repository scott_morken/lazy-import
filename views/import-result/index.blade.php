<?php
/**
 * @var \Smorken\LazyImport\Contracts\Models\ImportResult $model
 */

$filter = $viewModel->filter();
?>
<x-layouts.app>
    <x-smc::title>Import Results Administration</x-smc::title>
    @include('sm-lazyimport::import-result._filter_form')
    @if ($viewModel->models() && count($viewModel->models()))
        <x-smc::table>
            <x-slot:head>
                <x-smc::table.heading>ID</x-smc::table.heading>
                <x-smc::table.heading>Importer</x-smc::table.heading>
                <x-smc::table.heading>Run</x-smc::table.heading>
            </x-slot:head>
            <x-slot:body>
                @foreach ($viewModel->models() as $model)
                    @php($params = [...[$model->getKeyName() => $model->getKey()], ...$filter->toArray()])
                    <x-smc::table.row :model="$model">
                        <x-smc::table.cell>
                            <x-smc::resource.action.show
                                    title="View {{ $model->getKey() }}"
                                    :params="$params">{{ $model->getKey() }}</x-smc::resource.action.show>
                        </x-smc::table.cell>
                        <x-smc::table.cell>{{ $model->importer }}</x-smc::table.cell>
                        <x-smc::table.cell>{{ $model->created_at }}</x-smc::table.cell>
                    </x-smc::table.row>
                @endforeach
            </x-slot:body>
        </x-smc::table>
        <x-smc::paginate.models :models="$viewModel->models()" :filter="$filter"></x-smc::paginate.models>
    @else
        <div class="text-muted">No records found.</div>
    @endif
</x-layouts.app>
