<x-smc::form.qs-filter :filter="$filter">
    @php($helper = $component->helper)
    <x-smc::flex class="justify-content-start">
        <x-smc::input.group>
            <x-smc::input.label for="filter-importer" :visible="false">Importer</x-smc::input.label>
            <x-smc::input name="filter[importer]" class="{{ $helper->getClasses('importer') }}"
                          :value="$helper->getValue('importer')" placeholder="Importer"/>
        </x-smc::input.group>
        <x-smc::input.group class="input-group">
            <span class="input-group-text">Created Between...</span>
            <x-smc::input type="date" name="filter[createdAfter]" class="{{ $helper->getClasses('createdAfter') }}"
                          aria-label="Created After"
                          :value="$helper->getValue('createdAfter')"/>
            <x-smc::input type="date" name="filter[createdBefore]" class="{{ $helper->getClasses('createdBefore') }}"
                          aria-label="Created Before"
                          :value="$helper->getValue('createdBefore')" placeholder="created before"/>
        </x-smc::input.group>
        <x-smc::form.filter-buttons></x-smc::form.filter-buttons>
    </x-smc::flex>
</x-smc::form.qs-filter>
