<?php
/**
 * @var \Smorken\Domain\ViewModels\Contracts\RetrieveViewModel $viewModel
 * @var \Smorken\Support\Contracts\Filter|\Smorken\QueryStringFilter\Contracts\QueryStringFilter $filter
 * @var \Smorken\LazyImport\Contracts\Models\ImportResult $model
 */
use Smorken\Support\Filter;

$filter = $viewModel->filter() ?? new Filter();
$model = $viewModel->model();
?>
<x-layouts.app>
    @php($params = [...[$model->getKeyName() => $model->getKey()], ...$filter->toArray()])
    <x-smc::resource.title-back>Import Results Administration</x-smc::resource.title-back>
    <h5 class="mb-2">View record [{{ $model->getKey() }}]</h5>
    <x-smc::resource.part.show-row :model="$model" key="ID"
                                   attribute="id">{{ $model->id }}</x-smc::resource.part.show-row>
    <x-smc::resource.part.show-row :model="$model" key="Provider"
                                   attribute="name">{{ $model->importer }}</x-smc::resource.part.show-row>
    <x-smc::resource.part.show-row :model="$model" key="Run"
                                   attribute="type">{{ $model->created_at }}</x-smc::resource.part.show-row>
    <pre>{{ \Smorken\Support\Arr::stringify($model->data) }}</pre>
</x-layouts.app>
