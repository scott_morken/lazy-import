<?php

return [
    'actions' => [
        \Smorken\LazyImport\Contracts\Actions\DeleteImportResultsAction::class => \Smorken\LazyImport\Actions\DeleteImportResultsAction::class,
        \Smorken\LazyImport\Contracts\Actions\StoreImportResultsAction::class => \Smorken\LazyImport\Actions\StoreImportResultsAction::class,
    ],
    'models' => [
        \Smorken\LazyImport\Contracts\Models\ImportResult::class => \Smorken\LazyImport\Models\Eloquent\ImportResult::class,
    ],
    'repositories' => [
        \Smorken\LazyImport\Contracts\Repositories\FilteredImportResultsRepository::class => \Smorken\LazyImport\Repositories\FilteredImportResultsRepository::class,
        \Smorken\LazyImport\Contracts\Repositories\FindImportResultRepository::class => \Smorken\LazyImport\Repositories\FindImportResultRepository::class,
    ],
    'notifications' => [
        'to' => env('IMPORT_EMAIL_TO', env('ERROR_EMAIL')),
        'from' => env('IMPORT_EMAIL_FROM'),
    ],
    'migrations' => env('IMPORT_MIGRATIONS', true),
];
